FROM maven:3.8.5-openjdk-17 as build

WORKDIR /opt/app

COPY pom.xml .
#COPY settings.xml /usr/share/maven/ref/
#COPY ./src ./src
COPY ./idp-bridge ./idp-bridge
COPY ./idp-helper ./idp-helper
RUN mvn clean install

#FROM eclipse-temurin:17-jdk-alpine
FROM maven:3.8.5-openjdk-17
WORKDIR /app

COPY --from=build /opt/app/idp-bridge/target/idp-bridge-0.2.jar /app/idp-bridge.jar
COPY ./.env ./.env
#COPY ./config ./
#ENTRYPOINT ["java", "-jar", "revocation-registry.jar"]
#
#
#
#FROM eclipse-temurin:17-jdk-alpine
#COPY /target/idp-bridge-0.2.jar idp-bridge.jar
ENTRYPOINT ["java", "-jar", "idp-bridge.jar"]
