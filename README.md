- [Introduction to IDP Bridge](#idp-bridge)
- [IDP Bridge Architecture](#architecture-)
- [Authorization Server](#authorization-server)
  - [Client Registration](#dynamic-client-registration)
  - [Token Service](#token-service)
  - [User Session Management](#user-session-management)
  - [User Consent Service](#user-consent-service)
- [Verifier Manager](#verifier-manager)
  - [OID4VP & SIOPv2 protocols](#oid4vp--siopv2-protocols)
  - [Policy Evaluation Service](#policy-evaluation-service)
  - [Authentication Manager](#authentication-manager)
    - [OID4VP & SIOPv2](#oid4vp--siopv2)
    - [OIDC Brokering](#oidc-brokering)
    - [SSI-based authentication](#ssi-based-authentication)
    - [ID/Password Authentication](#idpassword-authentication)

## IDP Bridge
IDP Bridge aims at bridging the SSI and standard OIDC worlds by providing tools and services to allow user to request authorized verifiable credential in different formats and authenticate with a traditional IDP using their SSI Wallet. Whereas the former is achieved by implementing the OID4VCI protocol, we achieve the latter by implementing OID4VP/SIOPv2 protocols and providing the brokering service which is capable of interacting with OIDC-enabled traditional IDP.


## Architecture 
IDP Bridge comes with a highly modular and customizable software architecture. All components part of it comes with a default implementation which is customizable. When using IDP Bridge as dependency, you can bring your own implementation for some of the components and use the default implementation for other components with customization possibility. The customization is done through implementing Customizer interface of corresponding components. During application startup, IDP Bridge is capable of detecting all customizers and take them into account for building necessary components.



![Architecture](idp-bridge/documentation/architecture.png)

## Authorization Server

The authorization server implements multiple specifications to support the OIDC Core protocol. It is the core component which helps IDP Bridge securely connect to traditional IDP as well as do necessary steps to trigger ssi-based authentication protocols with ssi wallets. The  core components of Authorization Server is listed as follows:

- Client Registration
- Token Service
- Policy Evaluation Service
- User Session & Info Management
- User Consent Service

For more details about Authorization Server, please refer to [Documentation](/idp-bridge/documentation/README_AUTHORIZATION_SERVER.md)



## Verifier Manager
IDP Bridge supports multiple authentication mechanism via Authentication Manager. These protocols could interact with ssi wallet as well as other eIDs or user credentials. 
Moreover, these authentication mechanisms are also needed for credential issuance via *authorization_code* grant.

For more details about Verifier Manager, please refer to [Documentation](/idp-bridge/documentation/README_VERIFIER.md)

## Authentication Manager
In addition to SIOPv2 and OID4VP, IDP Bridge has supports for diverse authentication mechanism out of the box, including OIDC Brokering, ID/Password Authentication, PKI Card, and Authentication Aggregator. 

For more details about Authentication Manager and all supported authentication mechanisms, please refer to [Documentation](/idp-bridge/documentation/README_AUTH_MANAGER.md)


## Integration with Keycloak
Please refer to [Documentation](/idp-bridge/documentation/README_KEYCLOAK_INTEGRATION.md) for integration with Keycloak