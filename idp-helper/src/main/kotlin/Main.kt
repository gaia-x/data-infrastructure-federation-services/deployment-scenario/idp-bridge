package com.hoanhoang.idp.helper;
import id.walt.credentials.issuance.Issuer.mergingJwtIssue
import id.walt.credentials.issuance.Issuer.mergingSdJwtIssue
import id.walt.credentials.vc.vcs.W3CVC
import id.walt.credentials.verification.CredentialWrapperValidatorPolicy
import id.walt.credentials.verification.PolicyRunner
import id.walt.credentials.verification.models.PolicyRequest
import id.walt.credentials.verification.models.PresentationVerificationResponse
import id.walt.crypto.keys.*
import id.walt.crypto.utils.JsonUtils.toJsonElement
import id.walt.did.dids.DidService
import id.walt.did.dids.registrar.DidResult
import id.walt.did.dids.registrar.dids.DidCreateOptions
import id.walt.oid4vc.data.CredentialFormat
import id.walt.oid4vc.data.DurationInSecondsSerializer
import id.walt.oid4vc.providers.TokenTarget
import id.walt.oid4vc.responses.BatchCredentialResponse
import id.walt.oid4vc.responses.CredentialResponse
import id.walt.sdjwt.SDMap
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.serialization.json.*;
import java.util.concurrent.CompletableFuture
import kotlin.time.Duration
import kotlin.time.Duration.Companion.minutes
import kotlin.time.Duration.Companion.seconds;

fun main(args: Array<String>) {

    println("--------------IDP Helper-------------------")


}

object JsonHelper {

    fun createJSONPrimitive(value: Any): JsonPrimitive {
        return when(value){
            is String -> JsonPrimitive(value)
            is Number -> JsonPrimitive(value)
            is Boolean -> JsonPrimitive(value)
            else -> throw IllegalArgumentException("Unsupported type for JsonPrimitive")
        }
    }

    fun getContentFromJsonElement(json: JsonElement): String? {

        val value = when(json){
            is JsonObject -> json.toString()
            is JsonPrimitive -> json!!.jsonPrimitive.content
            null -> null
            else -> throw IllegalArgumentException("Illegal json value $json")
        }
        return value;
    }

    fun createJSONElement(value: String): JsonElement {
        return  Json.parseToJsonElement(value);
    }

    fun createJSONElementFromUnformattedValue(value: String): JsonElement{
        val formattedValue = "\"$value\""
        return Json.parseToJsonElement(formattedValue)
    }


    fun isJsonElementString(json: JsonElement) : Boolean {
        val value = when(json){
            is JsonPrimitive -> json.isString
            else -> false;
        }

        return value;
    }

    fun jsonObjectToMap(data: JsonObject) : Map<String, JsonElement>{
        return  data.toMap();
    }
}


object DateTimeHelper {
    fun createKotlinInstant(plusTime: Long): kotlinx.datetime.Instant {
        return Clock.System.now().plus(plusTime.seconds)
    }

    fun createKotlinInstant(duration: Duration): kotlinx.datetime.Instant{
        return Clock.System.now().plus(duration);
    }
}


object PolicyVerifierHelper {
    fun verifyPresentation(
        vpTokenJwt: String,
        vpPolicies: List<PolicyRequest>,
        globalVcPolicies: List<PolicyRequest>,
        specificCredentialPolicies: Map<String, List<PolicyRequest>>,
        presentationContext: Map<String, Any>
    ) : CompletableFuture<PresentationVerificationResponse> {

        val future = CompletableFuture<PresentationVerificationResponse>()
        GlobalScope.launch {
            try {
                val result = PolicyRunner.verifyPresentation(vpTokenJwt, vpPolicies, globalVcPolicies, specificCredentialPolicies, presentationContext);
                future.complete(result);
            } catch (e: Exception){
                println()
                future.completeExceptionally(e);
            }
        }

        return future;
    }
}


object DidServiceWrapper {

    fun initiateDidService(): CompletableFuture<Void>{
        val future = CompletableFuture<Void>();

        GlobalScope.launch {
            try {
                DidService.init();
                future.complete(null);
            } catch (e: Exception){
                future.completeExceptionally(e);
            }
        }
        return future
    }

    fun registerByKey(method: String, key: Key, metadata: Map<String, Any>): CompletableFuture<DidResult>{
        val future = CompletableFuture<DidResult>();
        val options: DidCreateOptions = DidCreateOptions(method, metadata)

        GlobalScope.launch {
            try{
                println("The did create options: ${options.method} and ${options.options}")
//                val didResult = DidService.registerByKey(method, key, options);
//                val didOptionMetadata =  mapOf("domain" to "eviden.com")
//                val didCreateOptions1 = DidCreateOptions(method, didOptionMetadata)
                val didResult = DidService.registerByKey(method, key, options)
                future.complete(didResult)
            } catch (e: Exception){
                future.completeExceptionally(e);
            }
        }
        return future;
    }

    fun resolveToKey(did: String): CompletableFuture<Key>{

        val future = CompletableFuture<Key>()
        GlobalScope.launch {
            try {
                val keyResult  = DidService.resolveToKey(did)
                val key =keyResult.getOrNull()
                future.complete(key)
            } catch (e: Exception){
                future.completeExceptionally(e);
            }
        }

        return future
    }
}

object KeyServiceWrapper {

    fun deserializeKey(keyObject: JsonObject): CompletableFuture<Key> {

        val future = CompletableFuture<Key>();
        GlobalScope.launch {
            try {
                val keyResult: Result<Key> =  KeySerialization.deserializeKey(keyObject);
                future.complete(keyResult.getOrNull());
            } catch (e: Exception){
                future.completeExceptionally(e);
            }
        }

        return future;

    }

    fun generateKey(type: KeyType, metadata: LocalKeyMetadata?): CompletableFuture<LocalKey>{
        val future = CompletableFuture<LocalKey>();

        GlobalScope.launch {
            try{
                val localKey = if(metadata == null) LocalKey.generate(type) else LocalKey.generate(type, metadata);

                future.complete(localKey);
            } catch (e: Exception){
                future.completeExceptionally(e)
            }
        }

        return future
    }

    fun signJws(localKey: Key, plaintext: ByteArray, headers: Map<String, String>): CompletableFuture<String>{
        val future =  CompletableFuture<String>();

        GlobalScope.launch {
            try{
                val signature = localKey.signJws(plaintext, headers);
                future.complete(signature);
            } catch (e: Exception){
                future.completeExceptionally(e);
            }
        }

        return future;
    }

    fun verifyJws(key: Key, signedJws: String): CompletableFuture<VerificationResult> {
        val future = CompletableFuture<VerificationResult>();
        GlobalScope.launch {
            try {
                val result = key.verifyJws(signedJws);
                if(result.isSuccess){
                    future.complete(VerificationResult(true, "Success"));
                } else {
                    future.complete(VerificationResult(false, "Signature failed"));
                }
            } catch (e: Exception){
                future.completeExceptionally(e)
            }
        }

        return future
    }
}

object VCServiceWrapper {

    fun mergingJwtIssuance(vc: W3CVC,
                           issuerKey: Key,
                           issuerDid: String,
                           subjectDid: String,
                           mappings: JsonObject,
                           additionalJwtHeader: Map<String, String>,
                           additionalJwtOptions: Map<String, JsonElement>,
                           completeJwtWithDefaultCredentialData: Boolean = true): CompletableFuture<String> {
        val future = CompletableFuture<String>();
        GlobalScope.launch {
            try {
                val result = vc.mergingJwtIssue(
                    issuerKey,
                    issuerDid,
                    subjectDid,
                    mappings,
                    additionalJwtHeader,
                    additionalJwtOptions,
                    completeJwtWithDefaultCredentialData)
                future.complete(result);
            } catch (e: Exception){
                future.completeExceptionally(e);
            }
        }
        return future;
    }

    fun mergingSdJwtIssue(vc: W3CVC,
                          issuerKey: Key,
                          issuerDid: String,
                          subjectDid: String,
                          mappings: JsonObject,

                          additionalJwtHeader: Map<String, String>,
                          additionalJwtOptions: Map<String, JsonElement>,

                          completeJwtWithDefaultCredentialData: Boolean = true,
                          disclosureMap: SDMap
    ) : CompletableFuture<String>{
        val future = CompletableFuture<String>();

        GlobalScope.launch {
            try{
                val result = vc.mergingSdJwtIssue(
                    issuerKey,
                    issuerDid,
                    subjectDid,
                    mappings,
                    additionalJwtHeader,
                    additionalJwtOptions,
                    completeJwtWithDefaultCredentialData,
                    disclosureMap);

                future.complete(result);


            } catch (e: Exception){
                future.completeExceptionally(e);
            }

        }
        return future;
    }


    fun credentialResponseSuccess(format: CredentialFormat,
                                  credential: JsonElement) = CredentialResponse.success(format, credential);

    fun batchCredentialResponseSuccess(credentialResponses: List<CredentialResponse>,
                                       cNonce: String? = null) = BatchCredentialResponse.success(credentialResponses, cNonce, 5.minutes);

}


abstract class JavaCredentialWrapperValidatorPolicy(name:String,
                                                    description: String,
                                                    private val isGlobalVCPolicy: Boolean,
                                                    private val isVPPolicy: Boolean
) : CredentialWrapperValidatorPolicy(name, description ){

    open fun isGlobalVCPolicy(): Boolean {
        return isGlobalVCPolicy;
    }

    open fun isVPPolicy(): Boolean {
        return isVPPolicy;
    }

    abstract fun doVerify(data: JsonObject, args: Any?, context: Map<String, Any>) : VerificationResult;

    override suspend fun verify(data: JsonObject, args: Any?, context: Map<String, Any>): Result<Any> {
        val result: VerificationResult = doVerify(data, args, context);

        return if(result.isSuccess){
            Result.success(Unit)
        } else{
            Result.failure(Exception(result.reason));
        }
    }
}

class VerificationResult(val isSuccess: Boolean, val reason: String)