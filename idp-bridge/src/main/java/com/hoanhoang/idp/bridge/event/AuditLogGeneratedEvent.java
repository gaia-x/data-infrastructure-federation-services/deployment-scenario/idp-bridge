package com.hoanhoang.idp.bridge.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.io.Serial;

@Getter
public class AuditLogGeneratedEvent extends ApplicationEvent {

    @Serial
    private static final long serialVersionUID = 6463145951476285158L;

    private final AuditLogDto auditLogDto;

    public AuditLogGeneratedEvent(AuditLogDto logDto){
        super(logDto);
        this.auditLogDto = logDto;
    }
}
