package com.hoanhoang.idp.bridge.service.oidc.model.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PolicyAssignmentRequest {

    @JsonProperty("policy_name")
    @NotNull
    private String policyName;

    @JsonProperty("credential_types")
    @NotEmpty
    private Set<String> vcTypes = new HashSet<>();
}
