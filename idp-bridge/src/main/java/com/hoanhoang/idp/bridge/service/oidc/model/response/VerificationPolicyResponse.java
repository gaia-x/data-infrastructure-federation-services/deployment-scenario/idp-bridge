package com.hoanhoang.idp.bridge.service.oidc.model.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hoanhoang.idp.bridge.entity.enums.PolicyStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerificationPolicyResponse {

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("isGlobalVCPolicy")
    private boolean isGlobalVCPolicy;

    @JsonProperty("isVPPolicy")
    private boolean isVPPolicy;

    @JsonProperty("status")
    private PolicyStatus status;

    @JsonProperty("appliedToVCs")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Set<String> appliedVCs = new HashSet<>();
}