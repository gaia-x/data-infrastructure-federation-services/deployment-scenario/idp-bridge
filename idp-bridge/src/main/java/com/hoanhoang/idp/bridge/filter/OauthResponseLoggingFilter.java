package com.hoanhoang.idp.bridge.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import java.io.IOException;


@Slf4j
public class OauthResponseLoggingFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(@NotNull HttpServletRequest request,
                                    @NotNull HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper(request);
        ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(response);

        filterChain.doFilter(requestWrapper, responseWrapper);
        logOAuthResponse(requestWrapper, responseWrapper);


    }

    private void logOAuthResponse(ContentCachingRequestWrapper requestWrapper, ContentCachingResponseWrapper responseWrapper){
        if(requestWrapper.getRequestURI().contains("userinfo")){
            try {
                log.info("UserInfo Request: {}", new String(requestWrapper.getContentAsByteArray()));
                log.info("UserInfo Response: {}", new String(responseWrapper.getContentAsByteArray()));
                responseWrapper.copyBodyToResponse();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

    }


    //    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//
//        if(request.getRequestURI().contains("token")) {
//            // Wrap the response to capture the token
//            ContentCachingResponseWrapper wrappedResponse = new ContentCachingResponseWrapper(response);
//            filterChain.doFilter(request, wrappedResponse);
//
//            // Extract and log the token
//            String responseBody = new String(wrappedResponse.getContentAsByteArray());
//            log.info("Token Endpoint Response: " + responseBody);
//
//            // Ensure the response is complete
//            wrappedResponse.copyBodyToResponse();
//        } else {
//            log.info("Could not found access token");
//            filterChain.doFilter(request, response);
//        }
//    }

}
