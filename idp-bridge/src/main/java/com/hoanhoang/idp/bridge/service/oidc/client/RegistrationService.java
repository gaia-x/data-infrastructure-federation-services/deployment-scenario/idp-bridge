package com.hoanhoang.idp.bridge.service.oidc.client;

import com.hoanhoang.idp.bridge.service.oidc.model.dto.ClientRegistrationDto;
import com.hoanhoang.idp.bridge.service.oidc.model.request.ClientRegistration;
import com.hoanhoang.idp.bridge.service.oidc.model.request.ClientRegistrationUpdate;
import com.hoanhoang.idp.bridge.service.oidc.model.response.ClientRegistrationResponse;

import java.util.List;

public interface RegistrationService {

    /**
     * Register a new client into IDP Bridge
     * @param registrationDto  a request containing client infos
     * @return client details
     */
    ClientRegistrationResponse registerClient(ClientRegistrationDto registrationDto);

    /**
     * Get details of a client by unique identifier
     * @param clientId unique identifier of the client
     * @return client details
     */
    ClientRegistrationResponse findRegisteredClientById(String clientId);



    /**
     * Find the details of all clients previously registered with authorization server
     * @return list of all clients details
     */
    List<ClientRegistrationResponse> findAllRegisteredClients();


    /**
     * Update the details of a client
     * @param clientRegistrationDto new details for previously registered client
     * @return client details
     */
    ClientRegistrationResponse updateRegisteredClient(ClientRegistrationDto clientRegistrationDto);


    /**
     * Delete a client previously registered in the authorization server
     * @param clientName unique identifier of client
     */
    void deleteRegisteredClient(String clientName);

}