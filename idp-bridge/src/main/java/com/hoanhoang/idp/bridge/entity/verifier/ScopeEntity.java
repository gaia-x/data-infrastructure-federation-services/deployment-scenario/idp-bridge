package com.hoanhoang.idp.bridge.entity.verifier;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(
        name = "oidc_default_scopes"
)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ScopeEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1398300288856701433L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToMany(
            fetch = FetchType.EAGER,
            cascade = { CascadeType.MERGE, CascadeType.PERSIST },
            mappedBy = "scopeEntities"
    )
    private Set<CredentialMappingEntity> credentialMappingEntities = new HashSet<>();

    public void addCredentialMapping(CredentialMappingEntity credentialMappingEntity){
        this.credentialMappingEntities.add(credentialMappingEntity);
        credentialMappingEntity.getScopeEntities().add(this);
    }

    public void removeCredentialMapping(CredentialMappingEntity credentialMappingEntity){
        this.credentialMappingEntities.remove(credentialMappingEntity);
        credentialMappingEntity.getScopeEntities().remove(this);
    }
}