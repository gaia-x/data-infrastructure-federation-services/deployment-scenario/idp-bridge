package com.hoanhoang.idp.bridge.service.verifier;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hoanhoang.idp.bridge.service.aster.AsterXVerifierService;
import com.hoanhoang.idp.helper.DateTimeHelper;
import com.hoanhoang.idp.helper.JsonHelper;
import com.hoanhoang.idp.helper.PolicyVerifierHelper;
import com.hoanhoang.idp.bridge.exception.ResourceNotFoundException;
import id.walt.credentials.verification.models.PolicyRequest;
import id.walt.credentials.verification.models.PresentationVerificationResponse;
import id.walt.crypto.utils.JwsUtils;
import id.walt.oid4vc.data.ResponseMode;
import id.walt.oid4vc.data.ResponseType;
import id.walt.oid4vc.data.dif.PresentationDefinition;
import id.walt.oid4vc.providers.CredentialVerifierConfig;
import id.walt.oid4vc.providers.OpenIDCredentialVerifier;
import id.walt.oid4vc.providers.PresentationSession;
import id.walt.oid4vc.requests.AuthorizationRequest;
import id.walt.oid4vc.responses.TokenResponse;
import kotlinx.serialization.json.JsonObject;
import kotlinx.serialization.json.JsonPrimitive;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;

@Service
@Slf4j
@Setter
@Getter
public class CredentialVerifierService extends OpenIDCredentialVerifier {

    private ConcurrentMap<String, PresentationSession> presentationSessions;    // Contains information related to the presentation:    sessionId <---> PresentationSession
    private ConcurrentMap<String, String> authenticatedSession;        // Contains information related to the authenticated session:    subject <-> sessionId
    private HashMap<String, SessionVerificationInformation> sessionVerificationInfos;  // Contains information related to policies to be verified on VP/VCs:    sessionId <---> sessionInformation
    private CredentialVerifierConfig verifierConfig;                  // Contains the configuration of verifier
    private Map<String, PresentationVerificationResponse> policyResults;    // Contains information related to the Verification Result
    private ObjectMapper objectMapper;
    private AsterXVerifierService asterXVerifierService;

    public CredentialVerifierService(CredentialVerifierConfig config, ObjectMapper objectMapper, AsterXVerifierService asterXVerifierService){
        super(config);
        this.verifierConfig = config;
        if(this.verifierConfig == null)
            throw new IllegalArgumentException("Failed to initiate the verifier service configuration");

        this.presentationSessions = new ConcurrentHashMap<>();
        this.authenticatedSession = new ConcurrentHashMap<>();
        this.policyResults = new HashMap<>();
        this.sessionVerificationInfos = new HashMap<>();
        this.objectMapper = objectMapper;
        this.asterXVerifierService = asterXVerifierService;
    }


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SessionVerificationInformation{
        private List<PolicyRequest> vpPolicies;
        private List<PolicyRequest> vcPolicies;
        private Map<String, List<PolicyRequest>> specificPolicies;
        private String successRedirectUri;
        private String errorRedirectUri;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CredentialPolicyResult{
        private String type;
        private List<JsonObject> policyResults;
    }

    @Nullable
    @Override
    public PresentationSession getSession(@NotNull String s) {
        return presentationSessions.get(s);
    }

    @Nullable
    @Override
    public PresentationSession putSession(@NotNull String s, PresentationSession presentationSession) {
        return presentationSessions.put(s, presentationSession);
    }

    @Nullable
    @Override
    public PresentationSession removeSession(@NotNull String s) {
        return presentationSessions.remove(s);
    }

    @NotNull
    @Override
    public PresentationSession verify(@NotNull TokenResponse tokenResponse, @NotNull PresentationSession session) {
        return super.verify(tokenResponse, session);
    }

    @Nullable
    @Override
    protected String preparePresentationDefinitionUri(@NotNull PresentationDefinition presentationDefinition, @NotNull String sessionID) {
        //return super.preparePresentationDefinitionUri(presentationDefinition, sessionID);
        //TODO: ADD an endpoint to expose the Presentation Definition based on sessionID
        log.warn("Preparing presentation definition uri for the sessionId: {}. Current status: Not implemented", sessionID);

        return null;
    }

    @Override
    protected boolean doVerify(@NotNull TokenResponse tokenResponse, @NotNull PresentationSession presentationSession) {
        return false;
    }

    @NotNull
    @Override
    protected String prepareResponseOrRedirectUri(@NotNull String sessionID, @NotNull ResponseMode responseMode) {
        String baseRedirectUri  = switch (responseMode){
            case query, fragment, form_post -> verifierConfig.getRedirectUri() != null ? verifierConfig.getRedirectUri() : verifierConfig.getClientId();
            default -> verifierConfig.getResponseUrl() != null ? verifierConfig.getResponseUrl(): verifierConfig.getClientId();
        };

        String redirectUri = baseRedirectUri + "/" + sessionID;
        //return super.prepareResponseOrRedirectUri(sessionID, responseMode);
        log.info("Preparing a response or redirect uri:  {}", redirectUri);

        return redirectUri ;
    }


    public PresentationSession verifyPresentation(TokenResponse tokenResponse,
                                                  PresentationSession presentationSession) {

        String sessionId = presentationSession.getId();
        log.info("Verifying the verifiable presentation for the session: {}", sessionId);
        SessionVerificationInformation policies = sessionVerificationInfos.get(sessionId);

        if(policies == null){
            log.error("Could not found any policies for the provided sessionId: {}", sessionId);
            throw new ResourceNotFoundException("Session","SessionID", sessionId);
        }

        Assert.notNull(tokenResponse.getVpToken(), "VP Token should not be NULL");
        String vpToken = JsonHelper.INSTANCE.getContentFromJsonElement(tokenResponse.getVpToken());

        if(vpToken == null || vpToken.isEmpty()){
            log.error("Could not find the vp_token in the TokenResponse");
            throw new IllegalArgumentException("Could not find the vp_token in the TokenResponse");
        }
        //log.info("Getting the vpToken: {} for the presentation session {}", vpToken, presentationSession.getId());
        CompletableFuture<PresentationVerificationResponse> future = PolicyVerifierHelper.INSTANCE.verifyPresentation(
                vpToken,
                policies.getVpPolicies(),
                policies.getVcPolicies(),
                policies.getSpecificPolicies(),
                Map.of("presentationDefinition", presentationSession.getPresentationDefinition(), "challenge", sessionId)
        );

        try {
            PresentationVerificationResponse verificationResponse = future.get();
            policyResults.put(presentationSession.getId(), verificationResponse);
            log.info("Overall Result for the presentation session: {}", verificationResponse.overallSuccess());

            if(!verificationResponse.overallSuccess()){
                verificationResponse.getResults().forEach(presentationResultEntry -> {
                    log.info("The verification result of verifiable credential: {}",presentationResultEntry.getCredential());
                    presentationResultEntry.getPolicyResults().forEach(policyResult -> {
                        log.info("Policy: {} has result: {}", policyResult.getRequest().getPolicy().getName(), policyResult.isSuccess());
                    });
                });
            }

            //Update the presentation session
            PresentationSession verifiedSession = new PresentationSession(
                    presentationSession.getId(),
                    presentationSession.getAuthorizationRequest(),
                    presentationSession.getExpirationTimestamp(),
                    presentationSession.getPresentationDefinition(),
                    tokenResponse,
                    verificationResponse.overallSuccess(),
                    presentationSession.getCustomParameters()
            );

            //Put User subject as key of the AuthenticatedSession Map
            if(verificationResponse.overallSuccess()){
                // Extract subject and save into the custom params of VerifiedSession
                String subject = getUsernameFor(vpToken);
                Assert.notNull(verifiedSession.getCustomParameters(), "CustomParameter should not be NULL");
                verifiedSession.getCustomParameters().put("username", subject);
                authenticatedSession.put(subject, sessionId);
            }
            presentationSessions.put(verifiedSession.getId(), verifiedSession);

            return verifiedSession;

        } catch (InterruptedException | ExecutionException e) {
            log.error("Failed to execute the VP Verification due to Exception: {} and Message: {}", e.getClass().toString(), e.getMessage());
            throw new RuntimeException(e);
        }
    }


    // Dedicated Verifying Service For Aster-X Conformity Assessment
    public PresentationSession verifyAsterXPresentation(TokenResponse tokenResponse, PresentationSession presentationSession) {

        String sessionId = presentationSession.getId();
        log.info("Verifying the verifiable presentation for the session: {}", sessionId);
        SessionVerificationInformation policies = sessionVerificationInfos.get(sessionId);

//        if(policies == null){
//            log.error("Could not found any policies for the provided sessionId: {}", sessionId);
//            throw new ResourceNotFoundException("Session","SessionID", sessionId);
//        }

        Assert.notNull(tokenResponse.getVpToken(), "VP Token should not be NULL");
        String vpToken = JsonHelper.INSTANCE.getContentFromJsonElement(tokenResponse.getVpToken());

        if(vpToken == null || vpToken.isEmpty()){
            log.error("Could not find the vp_token in the TokenResponse");
            throw new IllegalArgumentException("Could not find the vp_token in the TokenResponse");
        }


        // Verify vp_token according to Aster X Verification

//        AsterXPresentationService asterXPresentationService = new AsterXPresentationService();
//        asterXPresentationService.verifyJSONLDCredential()
//
//        //log.info("Getting the vpToken: {} for the presentation session {}", vpToken, presentationSession.getId());
//        CompletableFuture<PresentationVerificationResponse> future = PolicyVerifierHelper.INSTANCE.verifyPresentation(
//                vpToken,
//                policies.getVpPolicies(),
//                policies.getVcPolicies(),
//                policies.getSpecificPolicies(),
//                Map.of("presentationDefinition", presentationSession.getPresentationDefinition(), "challenge", sessionId)
//        );
//
//        try {
//            PresentationVerificationResponse verificationResponse = future.get();
//            policyResults.put(presentationSession.getId(), verificationResponse);
//            log.info("Overall Result for the presentation session: {}", verificationResponse.overallSuccess());
//
//            if(!verificationResponse.overallSuccess()){
//                verificationResponse.getResults().forEach(presentationResultEntry -> {
//                    log.info("The verification result of verifiable credential: {}",presentationResultEntry.getCredential());
//                    presentationResultEntry.getPolicyResults().forEach(policyResult -> {
//                        log.info("Policy: {} has result: {}", policyResult.getRequest().getPolicy().getName(), policyResult.isSuccess());
//                    });
//                });
//            }

            boolean verificationResponse = asterXVerifierService.verify(presentationSession.getPresentationDefinition(), tokenResponse);

            //Update the presentation session and add aster-x context
            Assert.notNull(presentationSession.getCustomParameters(), "CustomParams in presentation Session should NOT be NULL");
            presentationSession.getCustomParameters().put("context", "aster-x");

            PresentationSession verifiedSession = new PresentationSession(
                    presentationSession.getId(),
                    presentationSession.getAuthorizationRequest(),
                    presentationSession.getExpirationTimestamp(),
                    presentationSession.getPresentationDefinition(),
                    tokenResponse,
                    verificationResponse,
                    presentationSession.getCustomParameters()
            );

            //Put User subject as key of the AuthenticatedSession Map
            if(verificationResponse){
                // Extract subject and save into the custom params of VerifiedSession
                String subject = getUsernameFor(vpToken);
                Assert.notNull(verifiedSession.getCustomParameters(), "CustomParameter should not be NULL");
                verifiedSession.getCustomParameters().put("username", subject);
                authenticatedSession.put(subject, sessionId);
            }

            presentationSessions.put(verifiedSession.getId(), verifiedSession);

            return verifiedSession;
    }


    /// Initialize a presentation session
    public PresentationSession initializeAuthorization(PresentationDefinition presentationDefinition, ResponseMode responseMode, Set<String> scopes, Long expiresInSeconds){

        Map<String, Object> customParameters = new HashMap<>();
        customParameters.put("scopes", scopes);

        // Create a new presentation session
        PresentationSession session = new PresentationSession(
                UUID.randomUUID().toString(),
                null,
                DateTimeHelper.INSTANCE.createKotlinInstant(expiresInSeconds),
                presentationDefinition,
                null,
                null,
                customParameters
        );
        presentationSessions.put(session.getId(), session);
        // PreparePresentationDefinitionUri

        String presentationDefinitionUri = preparePresentationDefinitionUri(presentationDefinition, session.getId());
        String redirectUri = switch (responseMode){
            case query, fragment, form_post -> prepareResponseOrRedirectUri(session.getId(), responseMode);
            default -> null;
        };
        String responseUri = switch (responseMode){
            case direct_post -> prepareResponseOrRedirectUri(session.getId(), responseMode);
            default -> null;
        };

        AuthorizationRequest authorizationRequest = new AuthorizationRequest(
                ResponseType.Companion.getResponseTypeString(ResponseType.vp_token),
                verifierConfig.getClientId(),
                responseMode,
                redirectUri,
                scopes,
                session.getId(),
                null,
                null,
                null,
                null,
                null,
                presentationDefinitionUri == null ? presentationDefinition : null,
                presentationDefinitionUri,
                verifierConfig.getClientIdScheme(),
                null,
                null,
                null,
                responseUri,
                null,
                null,
                new HashMap<>()
        );

        log.info("Generating an authorization request: {}", authorizationRequest.toString());

        // Update the session in the ConcurrentHashMap
        PresentationSession updatedSession  = new PresentationSession(
                session.getId(),
                authorizationRequest,
                session.getExpirationTimestamp(),
                session.getPresentationDefinition(),
                session.getTokenResponse(),
                session.getVerificationResult(),
                session.getCustomParameters()
        );

        presentationSessions.put(updatedSession.getId(), updatedSession);
        return updatedSession;
    }

    private String getUsernameFor(String vp){
        JwsUtils.JwsParts jwsParts =  JwsUtils.INSTANCE.decodeJws(vp, true);
        String username = ((JsonPrimitive) Objects.requireNonNull(jwsParts.getPayload().get("iss"))).getContent();
        log.info("Extracting subject: [{}] for the provided vp", username);

        return username;
    }
}
