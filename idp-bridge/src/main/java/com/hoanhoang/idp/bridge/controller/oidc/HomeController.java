package com.hoanhoang.idp.bridge.controller.oidc;


import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller("idpHomeController")
@Hidden
@RequestMapping
@Slf4j
@Tag(
        name = "Favicon",
        description = "Favicon"
)
public class HomeController {

    private AuthenticationSuccessHandler successHandler = new SavedRequestAwareAuthenticationSuccessHandler();

    @GetMapping(
            path = "favicon.ico"
    )
    @ResponseBody
    public void favicon() {
        log.info("Loading a favicon.......");
    }


    @GetMapping(
            produces = { MediaType.TEXT_HTML_VALUE }
    )
    public ModelAndView landing(){
        log.info("Accessing the landing page");
        return new ModelAndView("landing");
    }

    @GetMapping(
            path = "/login",
            produces = MediaType.TEXT_HTML_VALUE
    )
    public ModelAndView loginPortal(@RequestParam(name = "scope", required = false) String scope){
        log.info("Redirect user to Authentication page with requested scopes {}:", scope);
        if(scope == null){
            scope = "";
        }
        ModelAndView modelAndView = new ModelAndView("login");
        modelAndView.addObject("scope", scope);
        return modelAndView;
    }

    @GetMapping(
            path = "/login-error",
            produces = MediaType.TEXT_HTML_VALUE
    )
    public ModelAndView loginError(@RequestParam(name = "scope", required = false) String scope){
        log.info("Redirect user to Authentication page with requested scopes {}:", scope);
        if(scope == null){
            scope = "";
        }
        ModelAndView modelAndView = new ModelAndView("login");
        modelAndView.addObject("scope", scope);
        modelAndView.addObject("param", Map.of("error", "Bad Credential"));
        return modelAndView;
    }

//    @GetMapping(
//            path = "/login"
//    )
//    public ModelAndView basicLogin(@RequestParam(value = "authentication", required = false) String authMode,
//                                   @RequestParam(value = "scope", required = false) String scope,
//                                   @RequestParam(value = "error", required = false) String error){
//
////        log.info("Authentication mode: {} & scope: {}", authMode, scope);
//        //String baseUrl = "http://127.0.0.1:8080/login";
////        String baseUrl = "http://localhost:8080/login";
//
////        String basicAuthUrl = (serverBaseUrl + "/login").replace("//","/");
//        String basicAuthUrl = "/login";
//        log.info("Redirecting user to basic login url: {}", basicAuthUrl);
//
////        return new ModelAndView("redirect:" + basicAuthUrl);
//        return new ModelAndView("login");
//    }

    @GetMapping(
            path = "/error",
            produces = { MediaType.TEXT_HTML_VALUE }
    )
    public ModelAndView error(){
        return new ModelAndView("error");
    }

}
