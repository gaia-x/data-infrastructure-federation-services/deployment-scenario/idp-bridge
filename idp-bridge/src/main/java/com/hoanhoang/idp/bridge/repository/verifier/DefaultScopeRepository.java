package com.hoanhoang.idp.bridge.repository.verifier;

import com.hoanhoang.idp.bridge.entity.verifier.ScopeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface DefaultScopeRepository extends JpaRepository<ScopeEntity, Long> {

    Optional<ScopeEntity> findByName(String name);
    Optional<ScopeEntity> findByIdOrName(Long id, String name);
    Set<ScopeEntity> findAllByNameIn(Set<String> names);

    @Query(value = "select scope.name from ScopeEntity scope")
    Set<String> findAllScopeNames();
}