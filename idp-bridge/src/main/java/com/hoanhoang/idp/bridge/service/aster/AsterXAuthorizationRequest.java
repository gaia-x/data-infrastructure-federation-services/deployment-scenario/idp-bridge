package com.hoanhoang.idp.bridge.service.aster;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AsterXAuthorizationRequest {

    @JsonProperty("authorization_request")
    private String authorizationRequest;
}