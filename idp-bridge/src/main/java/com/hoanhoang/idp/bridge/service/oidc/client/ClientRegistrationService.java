package com.hoanhoang.idp.bridge.service.oidc.client;

import com.hoanhoang.idp.bridge.authorizationserver.repository.ClientRepository;
import com.hoanhoang.idp.bridge.entity.oidc.ClientEntity;
import com.hoanhoang.idp.bridge.exception.ResourceNotFoundException;
import com.hoanhoang.idp.bridge.service.oidc.model.dto.ClientRegistrationDto;
import com.hoanhoang.idp.bridge.service.oidc.model.response.ClientRegistrationResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.time.Duration;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Slf4j
public class ClientRegistrationService implements RegistrationService {

    private final RegisteredClientRepository registeredClientRepository;
    private final ClientRepository clientRepository;

    @Value("${ssi.idp.security.token.lifespan}")
    private int accessTokenLifespan;

    @Value("${ssi.idp.security.code.lifespan}")
    private int authorizationCodeLifespan;

    @Value("${ssi.idp.client.consent.enabled}")
    private boolean enableConsentService;

    @Transactional
    @Override
    public ClientRegistrationResponse registerClient(ClientRegistrationDto registrationDto) {
        log.info("Registering a new client with name: {}", registrationDto.getClientName());
        if(clientRepository.existsByClientName(registrationDto.getClientName())){
           log.error("A client with name: {} has been previously registered", registrationDto.getClientName());
           throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("A client with name %s[] has been previously registered", registrationDto.getClientName()));
        }

        RegisteredClient registeredClient = convertToRegisteredClient(registrationDto);
        registeredClientRepository.save(registeredClient);

//        RegisteredClient savedClient = registeredClientRepository.findByClientId(registeredClient.getClientId());
        return registeredClientToClientRegistrationResponse(Objects.requireNonNull(registeredClientRepository.findByClientId(registeredClient.getClientId())));
    }


    private RegisteredClient convertToRegisteredClient(ClientRegistrationDto registrationDto){


        Set<AuthorizationGrantType> grantTypes = Set.of(
                AuthorizationGrantType.AUTHORIZATION_CODE,
                AuthorizationGrantType.CLIENT_CREDENTIALS,
                AuthorizationGrantType.REFRESH_TOKEN
        );

        Set<ClientAuthenticationMethod> clientAuthenticationMethods = Set.of(
                ClientAuthenticationMethod.CLIENT_SECRET_POST,
                ClientAuthenticationMethod.CLIENT_SECRET_BASIC
        );


        return RegisteredClient.withId(registrationDto.getClientName())
                .clientId(UUID.randomUUID().toString())
                .clientSecret("{noop}" + UUID.randomUUID().toString())
                .clientAuthenticationMethods( (methods) ->
                        methods.addAll(clientAuthenticationMethods))
                .authorizationGrantTypes(authorizationGrantTypes
                        -> authorizationGrantTypes.addAll(grantTypes))
                .scopes((scopes) -> scopes
                        .addAll(registrationDto.getScopes()))
                .redirectUris((uris) -> uris
                        .addAll(registrationDto.getRedirectUris()))
                .tokenSettings(TokenSettings.builder().accessTokenFormat(OAuth2TokenFormat.SELF_CONTAINED)
                        .accessTokenTimeToLive(Duration.ofMinutes(accessTokenLifespan))
                        .authorizationCodeTimeToLive(Duration.ofMinutes(authorizationCodeLifespan))
                        .build())
                .clientSettings(ClientSettings.builder().requireAuthorizationConsent(enableConsentService).build())
                .build();
    }

    @Transactional(readOnly = true)
    @Override
    public ClientRegistrationResponse findRegisteredClientById(String clientId) {
        log.info("Trying to find details of client with id: {}", clientId);
        RegisteredClient registeredClient = registeredClientRepository.findByClientId(clientId);

        if(registeredClient == null) {
            log.error("There is no registered client with id: {}", clientId);
            throw new ResourceNotFoundException("Client", "ClientId", clientId);
        }
        return registeredClientToClientRegistrationResponse(registeredClient);
    }


    @Transactional(readOnly = true)
    @Override
    public List<ClientRegistrationResponse> findAllRegisteredClients() {
         log.info("Finding details of all previously registered clients");
         List<String> clientIds = clientRepository.findAllClientIds();

         return clientIds.stream()
                 .map(this::findRegisteredClientById)
                 .toList();
    }


    @Transactional
    @Override
    public ClientRegistrationResponse updateRegisteredClient(ClientRegistrationDto clientRegistrationDto) {

        log.info("Updating a registered client with name: [{}]", clientRegistrationDto.getClientName());
        ClientEntity clientEntity = clientRepository.findByClientName(clientRegistrationDto.getClientName()).orElseThrow(() -> {
            log.error("Could not find Registered Client with name [{}] for update", clientRegistrationDto.getClientName());
            return new ResourceNotFoundException("Client", "Client name", clientRegistrationDto.getClientName());
        });

        RegisteredClient registeredClient = registeredClientRepository.findByClientId(clientEntity.getClientId());

        //clientEntity.setScopes(clientRegistrationDto.getScopes());
        if(!clientRegistrationDto.getScopes().isEmpty())
            clientEntity.setScopes(org.springframework.util.StringUtils.collectionToCommaDelimitedString(clientRegistrationDto.getScopes()));


        if(!clientRegistrationDto.getRedirectUris().isEmpty())
            clientEntity.setRedirectUris(org.springframework.util.StringUtils.collectionToCommaDelimitedString(clientRegistrationDto.getRedirectUris()));

        ClientEntity updatedClient = clientRepository.save(clientEntity);

        return findRegisteredClientById(updatedClient.getClientId());
    }


    @Transactional
    @Override
    public void deleteRegisteredClient(String clientName) {
        log.info("Deleting a registered client with name: [{}]", clientName);

        ClientEntity clientEntity = clientRepository.findByClientName(clientName).orElseThrow(() -> {
            log.error("Could not find a client with name: [{}] to delete", clientName);
            return new ResourceNotFoundException("Client", "client name", clientName);
        });

        clientRepository.delete(clientEntity);
        log.info("Successfully deleted client with name: {}", clientName);
    }

    private ClientRegistrationResponse registeredClientToClientRegistrationResponse(RegisteredClient registeredClient){

        return ClientRegistrationResponse.builder()
                .clientName(registeredClient.getClientName())
                .clientId(registeredClient.getClientId())
                .clientSecret(Objects.requireNonNull(registeredClient.getClientSecret()).replace("{noop}", ""))
                .redirectUris(registeredClient.getRedirectUris())
                .scopes(registeredClient.getScopes())
                .clientAuthenticationMethods(registeredClient.getClientAuthenticationMethods()
                        .stream()
                        .map(ClientAuthenticationMethod::getValue)
                        .collect(Collectors.toSet())
                )
                .build();
    }


    private String concatenateScopes(Set<String> scopes){
        StringBuilder builder = new StringBuilder();
        scopes.forEach( scope -> builder.append(scope).append(" "));
        String scope = StringUtils.removeEnd(builder.toString(), " ");
        log.info("Converting a set of scopes to a scoped string: {}", scope);
        return scope;
    }

    private AuthorizationGrantType authorizationGrantTypeFromString(String grantType){
        return switch (grantType){

            case "authorization_code" -> AuthorizationGrantType.AUTHORIZATION_CODE;
            case "refresh_token" -> AuthorizationGrantType.REFRESH_TOKEN;
            case "client_credentials" -> AuthorizationGrantType.CLIENT_CREDENTIALS;
            case "urn:ietf:params:oauth:grant-type:jwt-bearer" -> AuthorizationGrantType.JWT_BEARER;
            case "urn:ietf:params:oauth:grant-type:device_code" -> AuthorizationGrantType.DEVICE_CODE;

            default ->  throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("The specified grant type is not correct %s", grantType));
        };
    }
}

