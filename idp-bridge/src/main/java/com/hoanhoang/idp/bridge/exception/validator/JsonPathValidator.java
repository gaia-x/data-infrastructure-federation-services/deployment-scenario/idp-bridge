package com.hoanhoang.idp.bridge.exception.validator;

import com.jayway.jsonpath.JsonPath;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JsonPathValidator implements ConstraintValidator<ValidJsonPath, String> {

    @Override
    public void initialize(ValidJsonPath constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String jsonPath, ConstraintValidatorContext constraintValidatorContext) {

        if(jsonPath == null || jsonPath.isEmpty()){
            log.error("The provided JsonPath is null or empty");
            return false;
        }
        log.info("Validating the Jsonpath: {}", jsonPath);
        try {
            JsonPath.compile(jsonPath);
            return true;
        } catch (Exception e){
            log.error("The provided jsonpath: {} is not valid", jsonPath);
            return false;
        }
    }
}
