package com.hoanhoang.idp.bridge.authorizationserver.token;

import com.hoanhoang.idp.bridge.service.oidc.model.dto.CredentialMappingDto;
import com.hoanhoang.idp.bridge.service.oidc.model.dto.ScopeDto;
import com.hoanhoang.idp.bridge.service.oidc.scope.ScopeService;
import com.hoanhoang.idp.bridge.service.verifier.CredentialVerifierService;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import id.walt.crypto.utils.JwsUtils;
import id.walt.oid4vc.providers.PresentationSession;
import kotlinx.serialization.json.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;


/*
 * OAuthAuthenticatedUserInfoContext
 * Create OidcUserInfo object for all successful authentication session
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class OAuthAuthenticatedUserInfoContext {

    private final ScopeService scopeService;
    private final CredentialVerifierService credentialVerifierService;


    @SuppressWarnings("unchecked")
    public OidcUserInfo loadUserSessionInfo(String username){

        //Load the sessionId for the user to retrieve vp_token
        if(credentialVerifierService.getAuthenticatedSession().get(username) == null){
            log.info("Generating tokens for a non-oid4vp session for user: {} ", username);
            return OidcUserInfo.builder()
                    .subject(username)
                    .claim("vc_type", "BankId")      // Custom claim for testing purpose
                    .build();
        }

        log.info("Generating tokens for an oid4vp session for user: {} ", username);
        String sessionId = credentialVerifierService.getAuthenticatedSession().get(username);
        PresentationSession presentationSession = credentialVerifierService.getPresentationSessions().get(sessionId);

        Assert.notNull(presentationSession.getTokenResponse(), "Token response should not be NULL");
        Assert.notNull(presentationSession.getTokenResponse().getVpToken(), "vp token should not be NULL");
        String vpToken = ((JsonPrimitive)presentationSession.getTokenResponse().getVpToken()).getContent();

        Assert.notNull(presentationSession.getCustomParameters(), "Could not found scopes for this authentication session during loading user session info");
        Set<String> scopes = (Set<String>)presentationSession.getCustomParameters().get("scopes");

        log.info("Building user info profile with sessionId: {} and vp token: {} for required scopes: {}", sessionId, vpToken, scopes);
        // Get all scopes related to the authorization request
        scopes.removeIf(scope -> scope.equals("openid"));
        Map<String, Object> userInfo = new HashMap<>();
        scopes.forEach(scope -> userInfo.putAll(claimsForScope(scope, vpToken)));

        log.info("Generated userinfo is {}", userInfo);

        return OidcUserInfo.builder()
                .subject(username)
                .claims(claimsConsumer -> claimsConsumer.putAll(userInfo))
                .build();
    }

    public Map<String, CredentialMappingDto> claimMappingsForScope(String scope){
        // Get all the claim mappings for a given scope
        Map<String, CredentialMappingDto> claimMappings = new HashMap<>();

        ScopeDto scopeDto = scopeService.findScope(scope);
        scopeDto.getCredentialMappingDtos().forEach( mapping -> {
            String tokenClaim = mapping.getClaim();
            claimMappings.put(tokenClaim, mapping);
        });

        log.info("ClaimMappings for scope {} are: {}", scope, Arrays.toString(claimMappings.keySet().toArray()));
        return claimMappings;
    }

    public Map<String, Object> claimsForScope(String scope, String vpToken){
        // Get all the claims and associated vcPath for a given scope
        Map<String, CredentialMappingDto> claimMappings = claimMappingsForScope(scope);

        // Loop through all the vcs and try to get values for required claims
        JwsUtils.JwsParts jwsParts =  JwsUtils.INSTANCE.decodeJws(vpToken, true);

        JsonObject payload = jwsParts.getPayload();
        List<String> vcs = new ArrayList<>();

        JsonObject vpObject = (JsonObject) payload.get("vp");
        Assert.notNull(vpObject, "vpObject should not be NULL");
        if(vpObject.get("verifiableCredential") instanceof JsonArray jsonVCs){
            for(JsonElement jsonVc: jsonVCs){
                String vc = ((JsonPrimitive) jsonVc).getContent();
                log.info("Extracting  VC: {} from vp", vc);
                vcs.add(vc);
            }
        } else{
            log.error("Failed to decode vcs inside the vp token: {}", vpToken);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Failed to decode vcs inside the vp token");
        }

        Map<String, Object> userInfos = new HashMap<>();
        vcs.forEach( vc -> {
                    JwsUtils.JwsParts parts = JwsUtils.INSTANCE.decodeJws(vc, true);
                    JsonObject body = parts.getPayload();
                    // Convert it into a Json String to be used by Json Path
                    String bodyString = Json.Default.encodeToString(JsonElement.Companion.serializer(), body);
                    DocumentContext documentContext = JsonPath.parse(bodyString);

                    // Get the VC Type
                    String pathToVcType = "$.vc.type[*]";
                    List<String> vcTypes = documentContext.read(pathToVcType);

                    // Check if this vcType exists  is needed in any claim mapping
                    for(Map.Entry<String, CredentialMappingDto> mapping: claimMappings.entrySet()){
                        String tokenClaim = mapping.getKey();

                        if(userInfos.get(tokenClaim) == null && vcTypes.contains(mapping.getValue().getCredentialType())){
                            log.info("Extracting claim: {} from VC with type: {}", tokenClaim, Arrays.toString(vcTypes.toArray()));

                            String claimPath = mapping.getValue().getClaimPath();
                            List<String> subPaths = new ArrayList<>();

                            if(claimPath.contains(" ")){
                                subPaths.addAll(Arrays.stream(claimPath.split("\\s+")).toList()); // In case that two JSONString Path are put into one single claimPath like:    firstname lastname
                            } else {
                                subPaths.add(claimPath);
                            }

                            log.info("SubPaths  for the token claim: {} are:  {}", tokenClaim, Arrays.toString(subPaths.toArray()));
                            StringBuilder claimBuilder = new StringBuilder();
                            for(String subPath: subPaths){
                                String claimValue = documentContext.read(subPath);
                                claimBuilder.append(" ");
                                claimBuilder.append(claimValue);
                            }

                            String claim  = claimBuilder.toString().replaceFirst(" ","");
                            log.info("Extracted claim : {}", claim);
                            userInfos.put(tokenClaim, claim);
                        }
                    }
                });
        return userInfos;
    }

}