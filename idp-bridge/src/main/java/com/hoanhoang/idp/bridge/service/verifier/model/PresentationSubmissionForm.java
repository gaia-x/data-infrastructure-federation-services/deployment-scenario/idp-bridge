package com.hoanhoang.idp.bridge.service.verifier.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import id.walt.oid4vc.data.dif.VCFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PresentationSubmissionForm {

    @JsonProperty("id")
    private String id;

    @JsonProperty("descriptor_map")
    private List<PresentationDescriptor> definitionMap = new ArrayList<>();

    @JsonProperty("definition_id")
    private String definitionId;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PresentationDescriptor{

        @JsonProperty("id")
        private String id;

        @JsonProperty("path")
        private String path;

        @JsonProperty("format")
        private VCFormat vcFormat;
    }
}
