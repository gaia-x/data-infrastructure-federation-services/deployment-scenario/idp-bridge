package com.hoanhoang.idp.bridge.service.verifier.config;

import id.walt.oid4vc.data.ClientIdScheme;
import jakarta.annotation.Nullable;
import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "ssi.idp.service.verifier")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VerifierServiceConfig {

    @Nullable
    private String clientId;

    private ClientIdScheme clientIdScheme;

    @Nullable
    private String redirectUri;

    @Nullable
    private String responseUrl;
}
