package com.hoanhoang.idp.bridge.service.aster;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerifiablePresentationModel {

    // Define registered claims with
    @JsonProperty("@context")
    private Set<String> context = Set.of("https://www.w3.org/2018/credentials/v1");

    @JsonProperty("type")
    private Set<String> type = Set.of("VerifiablePresentation");

    @JsonProperty("verifiableCredential")
    private Set<Map<String, Object>> verifiableCredential;
}
