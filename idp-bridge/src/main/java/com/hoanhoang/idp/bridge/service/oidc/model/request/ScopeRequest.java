package com.hoanhoang.idp.bridge.service.oidc.model.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScopeRequest {

    @JsonProperty("name")
    @Nullable
    private String name;

    @JsonProperty("description")
    @Nullable
    private String description;
}
