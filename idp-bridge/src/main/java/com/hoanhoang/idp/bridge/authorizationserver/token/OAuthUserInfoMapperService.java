package com.hoanhoang.idp.bridge.authorizationserver.token;


import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.server.authorization.oidc.authentication.OidcUserInfoAuthenticationContext;
import org.springframework.security.oauth2.server.authorization.oidc.authentication.OidcUserInfoAuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;


/**
 * UserInfo Mapper Service
 * Enabling customization of response from userinfo endpoint for any authentication methods
 * This feature is needed as IDPBridge aims to work with non-standard scopes and claims
 * */

@Service
@Slf4j
@Getter
@Setter
public class OAuthUserInfoMapperService implements Function<OidcUserInfoAuthenticationContext, OidcUserInfo> {

    // In-memory storage for user claim mappings for all authentication methods
    private ConcurrentHashMap<String, Map<String, Object>> userClaimMappingsRepository;

    public OAuthUserInfoMapperService(){
        this.userClaimMappingsRepository = new ConcurrentHashMap<>();
    }

    public void addClaimMappingsForUser(String user, Map<String, Object> mappings){
        this.userClaimMappingsRepository.put(user, mappings);
    }

    @Override
    public OidcUserInfo apply(OidcUserInfoAuthenticationContext context) {
        OidcUserInfoAuthenticationToken authenticationToken = context.getAuthentication();
        JwtAuthenticationToken principal = (JwtAuthenticationToken) authenticationToken.getPrincipal();
        Map<String, Object> userInfoResponse = principal.getToken().getClaims();

        Map<String, Object> customUserInfoResponse = new HashMap<>(userInfoResponse);
        String userSub = (String)userInfoResponse.get("sub");

        Map<String, Object> userClaimMappings =  userClaimMappingsRepository.get(userSub);
        if(userClaimMappings != null)
            customUserInfoResponse.putAll(userClaimMappings);

        return new OidcUserInfo(customUserInfoResponse);
    }
}
