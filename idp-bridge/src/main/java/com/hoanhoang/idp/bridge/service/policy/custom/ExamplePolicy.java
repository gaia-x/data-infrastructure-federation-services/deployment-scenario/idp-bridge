package com.hoanhoang.idp.bridge.service.policy.custom;

import com.hoanhoang.idp.helper.JavaCredentialWrapperValidatorPolicy;
import com.hoanhoang.idp.helper.VerificationResult;
import kotlinx.serialization.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
@Slf4j
public class ExamplePolicy extends JavaCredentialWrapperValidatorPolicy {

    public ExamplePolicy() {
        super(
                "example-policy",
                "This is the description of the example-policy",
                false,
                false
        );
    }

    /**
     * @param data: contains vp_token
     * @param args: contains custom arguments which could be parsed by configuration file
     * @param context: contains information context in the authentication context such as presentation_submission
     * @return VerificationResult object containing the status and reason
     * */
    @NotNull
    @Override
    public VerificationResult doVerify(@NotNull JsonObject data, @Nullable Object args, @NotNull Map<String, ?> context) {
        log.info("Verifying submitted VCs against Example Policy");
        // TODO: With access to all the inputs such as vp_token, context information, the function implements the verification logic and return the corresponding result


        // TODO: If the verification failed, the function must return: new VerificationResult(false, "Failed to fulfill the verification logic")

        return new VerificationResult(true, "Success");
    }



}
