package com.hoanhoang.idp.bridge.controller.oidc;


import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;


@Tag(
        name = "Authentication Mechanism API Management",
        description = "Manage all configured authentication mechanisms"
)
@RequestMapping("/api/v1/auth")
@Slf4j
@RestController("idpAuthenticationController")
@Hidden
public class AuthenticationController {

    private final AuthenticationSuccessHandler successHandler = new SavedRequestAwareAuthenticationSuccessHandler();

    @GetMapping(
            path = "/finish"
    )
    public void finishAuthentication(HttpServletRequest request,
                                     HttpServletResponse response,
                                     Authentication authentication,
                                     @RequestParam(name = "key", required = false) String key){
        try {
            this.successHandler.onAuthenticationSuccess(request, response, authentication);
        } catch (ServletException | IOException e) {
            log.error("Got an error when trying to ServletException due to {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }

    @GetMapping(
            path = "/continue",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<?> continueAuth(@RequestParam(name = "key", required = false) String key,
                                          Authentication authentication){
        return ResponseEntity.ok().body(Map.of("status", "not authenticated. Please scan your QR Code"));
    }
}
