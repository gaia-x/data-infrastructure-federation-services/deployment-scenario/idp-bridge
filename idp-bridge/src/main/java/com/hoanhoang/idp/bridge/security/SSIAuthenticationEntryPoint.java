package com.hoanhoang.idp.bridge.security;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Map;


@Slf4j
public class SSIAuthenticationEntryPoint extends LoginUrlAuthenticationEntryPoint {

    public SSIAuthenticationEntryPoint(String loginPage){
        super(loginPage);
    }

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        StringBuilder  scopeBuilder = new StringBuilder();
        if(request.getRequestURI().contains("authorize")){
            Map <String, String[]> params =  request.getParameterMap();
            if(params.get("scope") != null){
                scopeBuilder.append(params.get("scope")[0]);
            } else {
                scopeBuilder.append("openid");
            }
        }
        UriComponents uriComponents = UriComponentsBuilder.fromUriString(getLoginFormUrl())
                .queryParam("scope", scopeBuilder.toString())
                .build();
        response.sendRedirect(uriComponents.toUriString());
    }
}