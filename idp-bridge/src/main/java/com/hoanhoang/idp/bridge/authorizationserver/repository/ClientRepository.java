package com.hoanhoang.idp.bridge.authorizationserver.repository;

import com.hoanhoang.idp.bridge.entity.oidc.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, String> {

    Optional<ClientEntity> findByClientId(String clientId);
    Optional<ClientEntity> findByClientName(String clientName);

    @Query(value = "select client.clientId from ClientEntity client")
    List<String> findAllClientIds();

    boolean existsByClientName(String clientName);
}
