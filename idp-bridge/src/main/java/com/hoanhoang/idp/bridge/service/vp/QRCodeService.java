package com.hoanhoang.idp.bridge.service.vp;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


@Slf4j
public class QRCodeService {

    public static byte[] generateQRCodeImage(String data){
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(data, BarcodeFormat.QR_CODE, 200,200);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            MatrixToImageWriter.writeToStream(bitMatrix, "PNG",byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();

        } catch (WriterException | IOException e) {
            log.error("Failed to generate a QR code from data: {} due to {}", data, ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }

    }
}
