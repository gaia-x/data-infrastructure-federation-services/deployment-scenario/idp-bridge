package com.hoanhoang.idp.bridge.service.aster;


import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AsterXVpTokenRequest {

    @NotEmpty
    private String issuer;

    @NotEmpty
    private String subject;

    @NotEmpty
    private List<Map<String, Object>> vcs;
}
