package com.hoanhoang.idp.bridge.entity.enums;


import lombok.Getter;
import lombok.Setter;

@Getter
public enum CredentialSchemaStatus {

    PENDING("PENDING"),
    ACTIVE("ACTIVE"),
    DISABLED("DISABLED");

    private final String status;

    CredentialSchemaStatus(String status){
        this.status = status;
    }
}
