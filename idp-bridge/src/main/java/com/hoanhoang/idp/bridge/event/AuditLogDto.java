package com.hoanhoang.idp.bridge.event;

import com.hoanhoang.idp.bridge.entity.audit.AuditActionConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.time.LocalDateTime;

/**
 * Audit Log Event
 * The class encompasses information related to action performed by user on a specific resource into a log event for audit purpose
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuditLogDto {

    private Long id;
    private String username;
    private AuditActionConstants action;
    private LocalDateTime createdAt;
    private String targetResource;
    private String details;
}
