package com.hoanhoang.idp.bridge.controller.user;


import com.hoanhoang.idp.bridge.service.user.base.UserCredentialDetailsProvider;
import com.hoanhoang.idp.bridge.service.user.impl.DefaultUserCredentialDetails;
import com.hoanhoang.idp.bridge.service.user.impl.DefaultUserCredentialDetailsProvider;
import com.hoanhoang.idp.bridge.service.user.model.UserCredentialDetails;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


@Tag(
        name = "User Management API",
        description = "Demo purpose only as the user model is minimal. One should create a specific user model depending on the use cases"
)
@Slf4j
@RequiredArgsConstructor
@RestController("idpUserController")
@RequestMapping("/users")
public class UserController {


    private final DefaultUserCredentialDetailsProvider userProvider;

    @Operation(
            summary = "Create a new user in information system",
            description = "Create a new user in information system"
    )
    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<?> createUser(@RequestBody @Valid DefaultUserCredentialDetails userDetails){
        UserCredentialDetails userCredentialDetails = userProvider.createUser(userDetails);
        return new ResponseEntity<>(userCredentialDetails, HttpStatus.CREATED);
    }

    @Operation(
            summary = "Find details of a user by username",
            description = "Find details of user by username "
    )
    @GetMapping(
            path = "/{username}",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<?> findUserByUsername(@PathVariable("username") String username){
        return new ResponseEntity<>(userProvider.getUserCredentialDetails(username), HttpStatus.OK);
    }

    @Operation(
            summary = "Find details of all user in the system",
            description = "Find details of all user in the system"
    )
    @GetMapping(
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<?> findAllUsers(){

        return new ResponseEntity<>(userProvider.findAllUser(), HttpStatus.OK);
    }

    @Operation(
            summary = "Delete user by username",
            description = "Delete user by username"
    )
    @DeleteMapping(
            path = "/{username}"
    )
    public ResponseEntity<?> deleteUserByUsername(@PathVariable("username") String username){

        if(!userProvider.deleteUser(username)){
            log.error("Failed to delete user with username: {}", username);
            throw new ResponseStatusException(HttpStatus.OK, String.format("Failed to delete user with name: %s", username));
        }
        return ResponseEntity.ok().build();
    }
}
