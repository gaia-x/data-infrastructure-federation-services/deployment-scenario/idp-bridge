package com.hoanhoang.idp.bridge.service.oidc.model.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientRegistrationUpdate {

    @JsonProperty("redirect_uris")
    private Set<String> redirectUris = new HashSet<>();

    @JsonProperty("scopes")
    private Set<String> scopes = new HashSet<>();
}
