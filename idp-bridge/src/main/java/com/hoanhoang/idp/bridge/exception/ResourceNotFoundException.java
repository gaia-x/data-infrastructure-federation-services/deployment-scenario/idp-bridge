package com.hoanhoang.idp.bridge.exception;


import lombok.Getter;
import lombok.Setter;

import java.io.Serial;

@Getter
@Setter
public class ResourceNotFoundException extends RuntimeException{
    @Serial
    private static final long serialVersionUID = -4236394172019254539L;

    private String resourceName;
    private String fieldName;
    private String fieldValue;

    public ResourceNotFoundException(String resourceName, String fieldName, String fieldValue){
        super(String.format("%s not found with %s: %s",resourceName, fieldName, fieldValue));
        this.resourceName = resourceName;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }
}
