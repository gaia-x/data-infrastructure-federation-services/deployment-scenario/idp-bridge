package com.hoanhoang.idp.bridge.config.converter;

import com.hoanhoang.idp.bridge.entity.enums.CredentialSchemaStatus;
import org.springframework.core.convert.converter.Converter;

public class CredentialStatusTypeConverter implements Converter<String, CredentialSchemaStatus> {

    @Override
    public CredentialSchemaStatus convert(String source) {
        return CredentialSchemaStatus.valueOf(source.toUpperCase());
    }
}
