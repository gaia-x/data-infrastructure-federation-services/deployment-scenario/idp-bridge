package com.hoanhoang.idp.bridge.service.policy.model;

import com.hoanhoang.idp.bridge.entity.enums.PolicyStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerificationPolicyDto {

    private String name;

    private String description;

    private boolean isGlobalVCPolicy;

    private boolean isVPPolicy;

    private PolicyStatus status;

    private Set<String> appliedVCs = new HashSet<>();
}
