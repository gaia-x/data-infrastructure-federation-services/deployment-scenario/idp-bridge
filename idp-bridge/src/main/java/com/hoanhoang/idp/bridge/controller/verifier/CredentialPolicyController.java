package com.hoanhoang.idp.bridge.controller.verifier;


import com.hoanhoang.idp.bridge.service.policy.model.VerificationPolicyDto;
import com.hoanhoang.idp.bridge.service.oidc.model.request.PolicyAssignmentRequest;
import com.hoanhoang.idp.bridge.service.oidc.model.response.VerificationPolicyResponse;
import com.hoanhoang.idp.bridge.service.policy.CredentialPolicyManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.util.List;
import java.util.stream.Collectors;


@Tag(
        name = "Verification Policy API",
        description = "Manage verification policies applied to verifiable credentials & verifiable presentations"
)
@RestController("idpCredentialPolicyController")
@Slf4j
@RequestMapping("/openid4vp")
@RequiredArgsConstructor
public class CredentialPolicyController {

    private final CredentialPolicyManager policyManager;
    private final ModelMapper modelMapper;

    @Operation(
            summary = "Get the details of a registered policy by name",
            description = "Get the detail of a registered policy by name" +
                    "\n\n The policy details specify its status of activation as well as VCs/VP that the policy IS applied to"
    )
    @GetMapping(
            path = "/policies/{name}",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<?> getPolicyByName(@PathVariable("name") String policyName){

        VerificationPolicyDto policyDto = policyManager.findVerificationPolicyByName(policyName);
        VerificationPolicyResponse policyResponse = modelMapper.map(policyDto, VerificationPolicyResponse.class);

        return new ResponseEntity<>(policyResponse, HttpStatus.OK);
    }


    @Operation(
            summary = "Get the details of all registered policies",
            description = "Get the details of all registered policies"
    )
    @GetMapping(
            path = "/policies",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<?> getRegisteredPolicies(){

        List<VerificationPolicyDto> policyDtos = policyManager.findAllVerificationPolicies();
        List<VerificationPolicyResponse> policyResponses = policyDtos.stream()
                .map(policyDto -> modelMapper.map(policyDto, VerificationPolicyResponse.class))
                .collect(Collectors.toList());

        return new ResponseEntity<>(policyResponses, HttpStatus.OK);
    }


    @Operation(
            summary = "Assign a custom policy to some VC types",
            description = "Assign a custom policy to some VC types" +
                    " \n\n Whenever a VC of the specified types is presented by Wallet, IDP Bridge verifies the VC against this policy"
    )
    @PostMapping(
            path = "/policies/assign",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<?> assignCustomPolicyToVC(@RequestBody @Valid PolicyAssignmentRequest request){

        VerificationPolicyDto updatedPolicy = policyManager.assignPolicyToVCTypes(request.getPolicyName(), request.getVcTypes());
        VerificationPolicyResponse policyResponse = modelMapper.map(updatedPolicy, VerificationPolicyResponse.class);

        return new ResponseEntity<>(policyResponse, HttpStatus.ACCEPTED);
    }


}
