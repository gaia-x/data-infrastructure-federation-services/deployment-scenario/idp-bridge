package com.hoanhoang.idp.bridge;

import com.hoanhoang.idp.helper.DidServiceWrapper;
import com.hoanhoang.idp.helper.KeyServiceWrapper;
import id.walt.crypto.keys.KeyType;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.concurrent.CompletableFuture;


@OpenAPIDefinition(
        info = @Info(
                title = "IDP Bridge",
                summary = "IDP Bridge for bridging the standard OIDC and SSI worlds",
                version = "1.0",
                contact = @Contact(
                        name = "Van Hoan HOANG",
                        email = "van-hoan.hoang@eviden.com"
                )
        )

)
@Slf4j
@SpringBootApplication
public class IDPBridgeApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(IDPBridgeApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Registering all DID registries for IDP Bridge ");

        CompletableFuture<Void> didService = DidServiceWrapper.INSTANCE.initiateDidService();
        didService.thenRun(() -> log.info("Successfully registered all DID Services"))
                .exceptionally(ex -> {
                    log.error("Failed to register DID Services due to: {}", ex.getMessage());
                    return null;
                });
        didService.get();
    }
}
