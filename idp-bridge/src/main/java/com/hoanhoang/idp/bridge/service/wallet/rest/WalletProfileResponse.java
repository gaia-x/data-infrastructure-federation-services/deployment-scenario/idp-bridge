package com.hoanhoang.idp.bridge.service.wallet.rest;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class WalletProfileResponse {

    private String provider;
    private String description;
    private Set<String> baseUrls = new HashSet<>();
    private Map<String, Object> customProperties = new HashMap<>();
}
