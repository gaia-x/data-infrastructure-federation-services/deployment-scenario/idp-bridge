package com.hoanhoang.idp.bridge.service.vp;

import com.hoanhoang.idp.bridge.service.verifier.model.CreateOID4VPSession;
import id.walt.oid4vc.data.ResponseMode;
import id.walt.oid4vc.data.dif.PresentationDefinition;

import java.util.Map;
import java.util.Set;

public interface PresentationManager {

    /**
     * Generate an authorization request from requested scopes
     * @param authorizeBaseUrl base authorization url
     * @param requestedScopes requested scopes sent from Client
     * @param responseMode See {@link ResponseMode}
     * @param successRedirectUri an uri to which Wallet is redirected to in case of authentication success
     * @param errorRedirectUri an uri to which Wallet is redirected to in case of authentication failure
     * @return an authorization request
     */
    String createAuthorizationRequestFromRequestedScopes(String authorizeBaseUrl, Set<String> requestedScopes, ResponseMode responseMode, String successRedirectUri, String errorRedirectUri);


    /**
     * Generate authorization request from requested VC Types
     * @param authorizeBaseUrl base authorization url
     * @param requestedCredentials requested credential types
     * @param requestedScopes requested scopes
     * @param responseMode response mode
     * @param successRedirectUri successRedirectUri
     * @param errorRedirectUri errorRedirectUri
     * @return authorization request
     */
    String createAuthorizationRequestFromRequestedCredentials(String authorizeBaseUrl, Set<String> requestedCredentials, Set<String> requestedScopes, ResponseMode responseMode, String successRedirectUri, String errorRedirectUri);

    /**
     * Generate QR Code for an authorization request
     * @param authorizeBaseUrl base authorization url
     * @param requestedCredentials requested credential types
     * @param requestedScopes requested scopes
     * @param responseMode response mode
     * @param successRedirectUri redirect uri in case of success authentication
     * @param errorRedirectUri redirect uri in case of failed authentication
     * @return QR code
     */
    byte[] createAuthorizationRequestQRCodeFromRequestedCredentials(String authorizeBaseUrl, Set<String> requestedCredentials, Set<String> requestedScopes, ResponseMode responseMode, String successRedirectUri, String errorRedirectUri);



    /**
     * Generate a presentation definition from requested credentials
     * @param requestedCredentialTypes a set of requested VC Types
     * @return a presentation_definition
     */
    PresentationDefinition fromRequestedCredentials(Set<String> requestedCredentialTypes);



    /**
     * Generate an authorization request during a vp authentication session
     * @param authorizeBaseUrl the base url for authorization
     * @param sessionRequest session request
     * @param responseMode See {@link ResponseMode}
     * @param successRedirectUri uri to which Wallet is redirected in case of authentication success
     * @param errorRedirectUri uri used by Wallet in case of failed authentication
     * @return an authorization request
     */
    String createAuthorizationRequest(String authorizeBaseUrl, CreateOID4VPSession sessionRequest, ResponseMode responseMode, String successRedirectUri, String errorRedirectUri);


    /**
     * Verify the validity of a given vp_token from Wallet
     * @param vpToken vp_token submitted by Wallets
     * @return validation result of the given vp_token
     */
    boolean verify(Map<String, Object> vpToken);
}