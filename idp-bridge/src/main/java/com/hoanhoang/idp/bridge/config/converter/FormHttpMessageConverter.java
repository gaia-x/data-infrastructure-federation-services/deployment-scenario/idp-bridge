package com.hoanhoang.idp.bridge.config.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hoanhoang.idp.bridge.service.verifier.model.PresentationSubmissionForm;
import com.hoanhoang.idp.bridge.service.verifier.model.TokenResponseForm;
import kotlinx.serialization.json.JsonElement;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.hoanhoang.idp.helper.JsonHelper;

@Slf4j
public class FormHttpMessageConverter extends AbstractHttpMessageConverter<TokenResponseForm> {

    public FormHttpMessageConverter(){
        super(MediaType.APPLICATION_FORM_URLENCODED);
    }

    @Override
    protected boolean supports(@NotNull Class<?> clazz) {
        return TokenResponseForm.class.equals(clazz);
    }

    @NotNull
    @Override
    protected TokenResponseForm readInternal(@NotNull Class<? extends TokenResponseForm> clazz, HttpInputMessage inputMessage) throws HttpMessageNotReadableException {
        Charset charset = Objects.requireNonNull(inputMessage.getHeaders().getContentType()).getCharset();

        charset = charset == null ? StandardCharsets.UTF_8 : charset;
        String formString = null;
        try {
            formString = StreamUtils.copyToString(inputMessage.getBody(), charset);
            Map<String, String> formMap = parseUrlEncoded(formString);
            ObjectMapper objectMapper = new ObjectMapper();

            String formFormatString = objectMapper.writeValueAsString(formMap);
            log.info("FormMapString:  {} ", formFormatString);


            JsonElement vpToken = JsonHelper.INSTANCE.createJSONElement(formMap.get("vp_token"));
            PresentationSubmissionForm presentationSubmissionForm = null;
            if(!StringUtils.isEmpty(formMap.get("presentation_submission"))){
                presentationSubmissionForm = objectMapper.readValue(formMap.get("presentation_submission"), PresentationSubmissionForm.class);
            }

            return new TokenResponseForm(vpToken, presentationSubmissionForm);
        } catch (IOException e) {
            log.error("Failed to reading the the token response from wallet due to {}", ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }


    private Map<String, String> parseUrlEncoded(String data) {
        return Arrays.stream(data.split("&"))
                .map(pair -> pair.split("="))
                .collect(Collectors.toMap(
                        pair -> URLDecoder.decode(pair[0], StandardCharsets.UTF_8),
                        pair -> pair.length > 1 ? URLDecoder.decode(pair[1], StandardCharsets.UTF_8) : "",
                        (value1, value2) -> value1 // In case of duplicates, keep the first one
                ));
    }

    @Override
    protected void writeInternal(@NotNull TokenResponseForm tokenResponseForm, @NotNull HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {

    }
}
