package com.hoanhoang.idp.bridge.authorizationserver.token;


import com.hoanhoang.idp.bridge.authorizationserver.config.AuthorizationServerConfiguration;
import com.hoanhoang.idp.bridge.service.aster.VerifiablePresentationModel;
import com.hoanhoang.idp.bridge.service.oidc.model.dto.CredentialMappingDto;
import com.hoanhoang.idp.bridge.service.oidc.model.dto.ScopeDto;
import com.hoanhoang.idp.bridge.service.oidc.scope.ScopeService;
import com.hoanhoang.idp.bridge.service.verifier.CredentialVerifierService;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimNames;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import id.walt.crypto.utils.JwsUtils;
import id.walt.oid4vc.providers.PresentationSession;
import kotlinx.serialization.json.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.server.ResponseStatusException;

import java.time.Instant;
import java.util.*;


/**
 * Aster-x dedicated token service for successfully authenticated user via Aster-X Membership
 * */
@Service
@Slf4j
@RequiredArgsConstructor
public class OAuthAsterTokenService {

    private final ScopeService scopeService;
    private final CredentialVerifierService credentialVerifierService;
    private final AuthorizationServerConfiguration authorizationServerConfiguration;

    @Value("${aster.service.token.expiration}")
    private long expirationInSeconds;

    @Value("${server.base-url}")
    private String tokenIssuer;
//    private final

//    @SuppressWarnings("unchecked")
    public OidcUserInfo loadUserSessionInfo(String username){

        log.info("Generating tokens for an aster-x oid4vp session for user: {} ", username);
        String sessionId = credentialVerifierService.getAuthenticatedSession().get(username);
        PresentationSession presentationSession = credentialVerifierService.getPresentationSessions().get(sessionId);

        Assert.notNull(presentationSession.getTokenResponse(), "Token response should not be NULL");
        Assert.notNull(presentationSession.getTokenResponse().getVpToken(), "vp token should not be NULL");
        String vpToken = ((JsonPrimitive)presentationSession.getTokenResponse().getVpToken()).getContent();

        Assert.notNull(presentationSession.getCustomParameters(), "Could not found scopes for this authentication session during loading user session info");
        Set<String> scopes = (Set<String>)presentationSession.getCustomParameters().get("scopes");

        log.info("Building user info profile with sessionId: {} and vp token: {} for required scopes: {}", sessionId, vpToken, scopes);
        // Get all scopes related to the authorization request
        scopes.removeIf(scope -> scope.equals("openid"));
        Map<String, Object> userInfo = new HashMap<>();
        scopes.forEach(scope -> userInfo.putAll(claimsForScope(scope, vpToken)));

        log.info("Generated userinfo is {}", userInfo);

        return OidcUserInfo.builder()
                .subject(username)
                .claims(claimsConsumer -> claimsConsumer.putAll(userInfo))
                .build();
    }

    public Map<String, CredentialMappingDto> claimMappingsForScope(String scope){
        // Get all the claim mappings for a given scope
        Map<String, CredentialMappingDto> claimMappings = new HashMap<>();

        ScopeDto scopeDto = scopeService.findScope(scope);
        scopeDto.getCredentialMappingDtos().forEach( mapping -> {
            String tokenClaim = mapping.getClaim();
            claimMappings.put(tokenClaim, mapping);
        });

        log.info("ClaimMappings for scope {} are: {}", scope, Arrays.toString(claimMappings.keySet().toArray()));
        return claimMappings;
    }

    public Map<String, Object> claimsForScope(String scope, String vpToken){
        // Get all the claims and associated vcPath for a given scope
        Map<String, CredentialMappingDto> claimMappings = claimMappingsForScope(scope);

        // Loop through all the vcs and try to get values for required claims
        JwsUtils.JwsParts jwsParts =  JwsUtils.INSTANCE.decodeJws(vpToken, true);

        JsonObject payload = jwsParts.getPayload();
        List<String> vcs = new ArrayList<>();

        JsonObject vpObject = (JsonObject) payload.get("vp");
        Assert.notNull(vpObject, "vpObject should not be NULL");
        if(vpObject.get("verifiableCredential") instanceof JsonArray jsonVCs){
            for(JsonElement jsonVc: jsonVCs){
                String vc = ((JsonObject) jsonVc).toString();
                log.info("Extracting  VC: {} from vp", vc);
                vcs.add(vc);
            }
        } else{
            log.error("Failed to decode vcs inside the vp token: {}", vpToken);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Failed to decode vcs inside the vp token");
        }

        Map<String, Object> userInfos = new HashMap<>();
        vcs.forEach( vc -> {
//            JwsUtils.JwsParts parts = JwsUtils.INSTANCE.decodeJws(vc, true);
//            JsonObject body = parts.getPayload();
//            // Convert it into a Json String to be used by Json Path
//            String bodyString = Json.Default.encodeToString(JsonElement.Companion.serializer(), body);
            DocumentContext documentContext = JsonPath.parse(vc);

            // Get the VC Type
            //String pathToVcType = "$.vc.type[*]";
            String pathToVcType = "$['credentialSubject']['@type']";
            String vcType = documentContext.read(pathToVcType);


            // Check if this vcType exists  is needed in any claim mapping
            for(Map.Entry<String, CredentialMappingDto> mapping: claimMappings.entrySet()){
                String tokenClaim = mapping.getKey();

                if(userInfos.get(tokenClaim) == null && vcType.equals(mapping.getValue().getCredentialType())){
                    log.info("Extracting claim: {} from VC with type: {}", tokenClaim, vcType);

                    String claimPath = mapping.getValue().getClaimPath();
                    List<String> subPaths = new ArrayList<>();

                    if(claimPath.contains(" ")){
                        subPaths.addAll(Arrays.stream(claimPath.split("\\s+")).toList()); // In case that two JSONString Path are put into one single claimPath like:    firstname lastname
                    } else {
                        subPaths.add(claimPath);
                    }

                    log.info("SubPaths  for the token claim: {} are:  {}", tokenClaim, Arrays.toString(subPaths.toArray()));
                    StringBuilder claimBuilder = new StringBuilder();
                    for(String subPath: subPaths){
                        String claimValue = documentContext.read(subPath);
                        claimBuilder.append(" ");
                        claimBuilder.append(claimValue);
                    }

                    String claim  = claimBuilder.toString().replaceFirst(" ","");
                    log.info("Extracted claim : {}", claim);
                    userInfos.put(tokenClaim, claim);
                }
            }
        });
        return userInfos;
    }


    public String issue(String username, Set<String> scopes){

        try {
            OidcUserInfo oidcUserInfo = loadUserSessionInfo(username);
            RSAKey rsaKey = authorizationServerConfiguration.getRsaKey();

            log.info("Generating an access token for {} with did: {}", username, oidcUserInfo.getSubject());
            JWSSigner signer = new RSASSASigner(rsaKey);
            JWTClaimsSet.Builder jwtClaimsSetBuilder = new JWTClaimsSet.Builder()
                    .issuer(tokenIssuer)
                    .subject(oidcUserInfo.getSubject())
                    .issueTime(new Date())
                    .expirationTime(new Date(new Date().getTime() + expirationInSeconds * 1000))
                    .jwtID(UUID.randomUUID().toString())
                    .notBeforeTime(new Date())
                    .claim("typ", "Bearer")
                    .claim("custom_claim","API-GateWay");

            if(scopes != null) {
                scopes.add("openid");
                Collections.reverse(new ArrayList<>(scopes.stream().toList()));
                String delimitedScopes = String.join(" ", scopes);
                jwtClaimsSetBuilder.claim("scope", delimitedScopes);

                log.error("Found scopes: {}", delimitedScopes);
            } else {
                log.error("Could not find any scopes");
            }

            oidcUserInfo.getClaims().forEach(jwtClaimsSetBuilder::claim);

            JWTClaimsSet jwtClaimsSet = jwtClaimsSetBuilder.build();
            JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.parse(rsaKey.getAlgorithm().getName()))
                    .type(JOSEObjectType.JWT)
                    .keyID(rsaKey.getKeyID())
                    .build();

            SignedJWT signedJWT = new SignedJWT(jwsHeader, jwtClaimsSet);
            signedJWT.sign(signer);

            return signedJWT.serialize();

        } catch (JOSEException e) {
            log.error("Failed to issue access token for {} due to {}", username, ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }


    public String issueVPToken(String iss, String subject, VerifiablePresentationModel vp){

        try {
//            OidcUserInfo oidcUserInfo = loadUserSessionInfo(username);
            RSAKey rsaKey = authorizationServerConfiguration.getRsaKey();

            JWSSigner signer = new RSASSASigner(rsaKey);
            JWTClaimsSet.Builder jwtClaimsSetBuilder = new JWTClaimsSet.Builder()
                    .issuer(iss)
                    .subject(subject)
                    .issueTime(new Date())
                    .expirationTime(new Date(new Date().getTime() + expirationInSeconds * 1000))
                    .jwtID(UUID.randomUUID().toString())
                    .notBeforeTime(new Date())
                    .claim("custom_claim","API-GateWay")
                    .claim("vp", vp);

            JWTClaimsSet jwtClaimsSet = jwtClaimsSetBuilder.build();
            JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.parse(rsaKey.getAlgorithm().getName()))
                    .type(JOSEObjectType.JWT)
                    .keyID(rsaKey.getKeyID())
                    .build();

            SignedJWT signedJWT = new SignedJWT(jwsHeader, jwtClaimsSet);
            signedJWT.sign(signer);

            return signedJWT.serialize();

        } catch (JOSEException e) {
            log.error("Failed to issue access token for {} due to {}", iss, ExceptionUtils.getMessage(e));
            throw new RuntimeException(e);
        }
    }

//    public String issue(){
//
//        try {
////            OidcUserInfo oidcUserInfo = loadUserSessionInfo(username);
//            RSAKey rsaKey = authorizationServerConfiguration.getRsaKey();
////            log.info("PublicKey: {}", rsaKey.g);
//            JWSSigner signer = new RSASSASigner(rsaKey);
//            JWTClaimsSet.Builder jwtClaimsSetBuilder = new JWTClaimsSet.Builder()
//                    .subject("example_subject")
//                    .issueTime(new Date())
//                    .expirationTime(new Date(new Date().getTime() + expirationInSeconds * 1000))
//                    .jwtID(UUID.randomUUID().toString())
//                    .claim("custom_claim","API-GateWay");
//
////            oidcUserInfo.getClaims().forEach(jwtClaimsSetBuilder::claim);
//
//            JWTClaimsSet jwtClaimsSet = jwtClaimsSetBuilder.build();
//            JWSHeader jwsHeader = new JWSHeader.Builder(JWSAlgorithm.parse(rsaKey.getAlgorithm().getName()))
//                    .type(JOSEObjectType.JWT)
//                    .keyID(rsaKey.getKeyID())
//                    .build();
//
//            SignedJWT signedJWT = new SignedJWT(jwsHeader, jwtClaimsSet);
//            signedJWT.sign(signer);
//
//            return signedJWT.serialize();
//
//        } catch (JOSEException e) {
//            log.error("Failed to issue access token for {} due to {}", "example-subject", ExceptionUtils.getMessage(e));
//            throw new RuntimeException(e);
//        }
//    }




}
