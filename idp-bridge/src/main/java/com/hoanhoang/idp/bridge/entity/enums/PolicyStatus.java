package com.hoanhoang.idp.bridge.entity.enums;

public enum PolicyStatus {

    ACTIVE("ACTIVE"),
    DISABLED("DISABLED");

    private final String status;

    PolicyStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return this.status;
    }
}