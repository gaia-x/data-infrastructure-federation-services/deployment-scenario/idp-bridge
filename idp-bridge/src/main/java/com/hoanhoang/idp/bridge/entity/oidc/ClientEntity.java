package com.hoanhoang.idp.bridge.entity.oidc;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(
        name = "oidc_clients"
)
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class ClientEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -3366726016446414924L;

    @Id
    private String id;

    @Column(unique = true)
    private String clientId;

    @Column(unique = true)
    private String clientSecret;

    private Instant clientIdIssuedAt;
    private Instant clientSecretExpiresAt;
    private String clientName;
    @Column(length = 1000)
    private String clientAuthenticationMethods;
    @Column(length = 1000)
    private String authorizationGrantTypes;
    @Column(length = 1000)
    private String redirectUris;
    @Column(length = 1000)
    private String postLogoutRedirectUris;
    @Column(length = 1000)
    private String scopes;
    @Column(length = 2000)
    private String clientSettings;
    @Column(length = 2000)
    private String tokenSettings;
}
