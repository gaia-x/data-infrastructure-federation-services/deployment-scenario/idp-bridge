package com.hoanhoang.idp.bridge.service.oidc.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientRegistrationDto {

    private String clientName;
    private Set<String> redirectUris = new HashSet<>();
    private Set<String> scopes = new HashSet<>();
}