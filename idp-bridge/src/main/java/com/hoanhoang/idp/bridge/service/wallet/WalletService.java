package com.hoanhoang.idp.bridge.service.wallet;

import com.hoanhoang.idp.bridge.service.wallet.model.WalletProfileDto;

import java.util.List;

public interface WalletService {

    WalletProfileDto createWalletProfile(WalletProfileDto profileDto);
    List<WalletProfileDto> findAllWalletProfiles();
    WalletProfileDto findWalletProfileByIdOrProvider(Long walletId, String walletProvider);
    void deleteWalletProfile(Long walletId, String walletProvider);
}