package com.hoanhoang.idp.bridge.service.oidc.model.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IATClientRegistration {

    @JsonProperty("client_name")
    private String clientName;

    @JsonProperty("grant_types")
    private List<String> granTypes;

    @JsonProperty("redirect_uris")
    private List<String> redirectUris;

    @JsonProperty("logo_uri")
    private String logoUri;

    @JsonProperty("contacts")
    private List<String> contacts;

    @JsonProperty("scope")
    private String scope;

}
