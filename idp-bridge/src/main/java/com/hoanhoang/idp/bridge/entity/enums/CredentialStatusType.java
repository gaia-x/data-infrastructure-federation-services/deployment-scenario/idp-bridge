package com.hoanhoang.idp.bridge.entity.enums;


import lombok.Getter;

@Getter
public enum CredentialStatusType {

    REVOCATION("REVOCATION"),
    SUSPENSE("SUSPENSE");

    private final String type;

    CredentialStatusType(String type) {
        this.type = type;
    }
}
