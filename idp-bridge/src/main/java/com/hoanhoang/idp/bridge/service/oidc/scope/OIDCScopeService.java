package com.hoanhoang.idp.bridge.service.oidc.scope;


import com.hoanhoang.idp.bridge.service.oidc.model.dto.CredentialMappingDto;
import com.hoanhoang.idp.bridge.service.oidc.model.dto.ScopeDto;
import com.hoanhoang.idp.bridge.entity.verifier.CredentialMappingEntity;
import com.hoanhoang.idp.bridge.entity.verifier.ScopeEntity;
import com.hoanhoang.idp.bridge.exception.ResourceNotFoundException;
import com.hoanhoang.idp.bridge.repository.verifier.DefaultCredentialMappingRepository;
import com.hoanhoang.idp.bridge.repository.verifier.DefaultScopeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;


import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class OIDCScopeService implements ScopeService{

    private final DefaultScopeRepository defaultScopeRepository;
    private final DefaultCredentialMappingRepository vcMappingRepository;
    private final ModelMapper modelMapper;

    private boolean verifyScopeExistence(Long id, String name){
        Optional<ScopeEntity> scopeEntity = defaultScopeRepository.findByIdOrName(id, name);
        return scopeEntity.isPresent();
    }

    @Transactional
    @Override
    public ScopeDto addScope(ScopeDto scopeDto) {
        if (verifyScopeExistence(null, scopeDto.getName())){
            log.error("A scope with name [{}] has existed", scopeDto.getName());
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("A scope with name %s has existed", scopeDto.getName()));
        }

        ScopeEntity scopeEntity = modelMapper.map(scopeDto, ScopeEntity.class);
        ScopeEntity createdScope = defaultScopeRepository.save(scopeEntity);
        return modelMapper.map(createdScope, ScopeDto.class);
    }

    @Transactional
    @Override
    public void removeScopeMappings(String scopeName, boolean mappingOnly) {
        if(!verifyScopeExistence(null, scopeName)){
            log.error("Could not find a scope with name [{}]", scopeName);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Could not find a scope with name %s", scopeName));
        }
        ScopeEntity scopeEntity = defaultScopeRepository.findByName(scopeName).get();
        Set<CredentialMappingEntity> credentialMappingEntities = scopeEntity.getCredentialMappingEntities();

        Set<CredentialMappingEntity> mappingsToRemove = new HashSet<>(credentialMappingEntities);

        mappingsToRemove.forEach(scopeEntity::removeCredentialMapping);
        if(mappingOnly)
            vcMappingRepository.deleteAll(mappingsToRemove);
        else
            defaultScopeRepository.delete(scopeEntity);
    }

    @Transactional(readOnly = true)
    @Override
    public ScopeDto findScope(String scopeName) {
        ScopeEntity scopeEntity = defaultScopeRepository.findByName(scopeName)
                .orElseThrow(() -> new ResourceNotFoundException("Scope", "Scope name", scopeName));

        Set<CredentialMappingEntity> credentialMappingEntities = scopeEntity.getCredentialMappingEntities();
        ScopeDto scopeDto = new ScopeDto();
        scopeDto.setName(scopeEntity.getName());
        scopeDto.setDescription(scopeEntity.getDescription());
        credentialMappingEntities.forEach(credentialMappingEntity -> {
            CredentialMappingDto credentialMappingDto = modelMapper.map(credentialMappingEntity, CredentialMappingDto.class);
            scopeDto.getCredentialMappingDtos().add(credentialMappingDto);
        });

        return scopeDto;
    }

    @Transactional(readOnly = true)
    @Override
    public Set<ScopeDto> findAllScopes() {
        Set<String> scopeNames = defaultScopeRepository.findAllScopeNames();

        return scopeNames.stream()
                .map(this::findScope)
                .collect(Collectors.toSet());
    }

    @Transactional(readOnly = true)
    @Override
    public Set<String> toCredentialTypes(Set<String> scopeNames) {
        // Remove default scope openid if included
        scopeNames.remove("openid");

        Set<String> supportedScopeNames = defaultScopeRepository.findAllScopeNames();
        Set<String> unMatchedScopes = new HashSet<>(scopeNames);
        unMatchedScopes.removeAll(supportedScopeNames);

        if( ! unMatchedScopes.isEmpty()){
            log.error("The following scopes are not defined in IDP Bridge: {}", unMatchedScopes);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("The following scopes are not defined in IDP Bridge: [%s]", unMatchedScopes));
        }
//        if(scopes.isEmpty()){
//            log.error("The following scopes are not defined in IDP Bridge: {}", scopes);
//            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("The following scopes are not defined in IDP Bridge: [%s]", scopes));
//        }
        Set<ScopeEntity> scopes = defaultScopeRepository.findAllByNameIn(scopeNames);

        return scopes.stream()
                .flatMap(scopeEntity -> scopeEntity.getCredentialMappingEntities().stream())
                .map(CredentialMappingEntity::getCredentialType)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<String> fromCredentialTypes(Set<String> credentialTypes) {
        return null;
    }

    @Transactional
    @Override
    public void addCredentialMapping(String scopeName, Set<CredentialMappingDto> credentialMappingDtos) {
        ScopeEntity scopeEntity = defaultScopeRepository.findByName(scopeName).orElseThrow(() ->
                new ResourceNotFoundException("Scope", "Scope Name", scopeName));

        credentialMappingDtos.forEach(credentialMappingDto -> {
            CredentialMappingEntity credentialMappingEntity = modelMapper.map(credentialMappingDto, CredentialMappingEntity.class);
            scopeEntity.addCredentialMapping(credentialMappingEntity);
        });
        defaultScopeRepository.save(scopeEntity);
    }

    @Transactional
    @Override
    public void removeCredentialMapping(String scopeName, Set<Long> mappingIds) {
        ScopeEntity scopeEntity = defaultScopeRepository.findByName(scopeName)
                .orElseThrow(() -> new ResourceNotFoundException("Scope", "Scope Name", scopeName));

        List<CredentialMappingEntity> credentialMappingEntities = vcMappingRepository.findAllById(mappingIds);
        credentialMappingEntities.forEach(scopeEntity::removeCredentialMapping);

        defaultScopeRepository.save(scopeEntity);

    }
}
