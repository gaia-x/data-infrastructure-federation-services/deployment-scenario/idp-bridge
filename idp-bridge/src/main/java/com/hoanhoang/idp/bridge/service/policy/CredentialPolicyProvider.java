package com.hoanhoang.idp.bridge.service.policy;


import com.hoanhoang.idp.helper.JavaCredentialWrapperValidatorPolicy;
import com.hoanhoang.idp.bridge.service.policy.model.VerificationPolicyDto;
import com.hoanhoang.idp.bridge.entity.verifier.VerificationPolicyEntity;
import com.hoanhoang.idp.bridge.entity.enums.PolicyStatus;
import com.hoanhoang.idp.bridge.exception.ResourceNotFoundException;
import com.hoanhoang.idp.bridge.repository.verifier.DefaultVerificationPolicyRepository;
import jakarta.annotation.PostConstruct;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@Data
public class CredentialPolicyProvider implements CredentialPolicyManager{

    private final DefaultVerificationPolicyRepository policyRepository;
    private final List<JavaCredentialWrapperValidatorPolicy> policies;
    private final ModelMapper modelMapper;

    @Transactional
    @PostConstruct
    public void boostrapVerificationPolicies(){
        if(policyRepository.count() == 0 ){
            log.info("Verifiable Policy Table is empty, try to register default policies");
            VerificationPolicyEntity signaturePolicy = VerificationPolicyEntity.builder()
                    .name("signature")
                    .description("Verify the signature on vc and vp presented by Wallet")
                    .isGlobalVCPolicy(true)
                    .isVPPolicy(true)
                    .status(PolicyStatus.ACTIVE)
                    .build();

            VerificationPolicyEntity expirationPolicy = VerificationPolicyEntity.builder()
                    .name("expired")
                    .description("Verify whether vc and vp are already expired")
                    .isVPPolicy(false)
                    .isGlobalVCPolicy(true)
                    .status(PolicyStatus.ACTIVE)
                    .build();

            policyRepository.saveAll(List.of(signaturePolicy, expirationPolicy));
        }


        List<String> policyNames = policies.stream()
                .map(policy -> {
                    log.info("Policy {} is of type {}", policy.getName(), policy instanceof PresentationDefinitionPolicy ? "Presentation Definition": "Could not defined bean class");
                    return policy.getName();
                })
                .toList();


        log.info("Registering custom policies: {} ", Arrays.toString(policyNames.toArray()));

        List<VerificationPolicyEntity> customPolicies = policies.stream()
                .filter(policy -> !policyRepository.existsByName(policy.getName()))
                .map(policy -> {
                    VerificationPolicyEntity policyEntity = VerificationPolicyEntity.builder()
                            .name(policy.getName())
                            .description(policy.getDescription())
                            .isGlobalVCPolicy(policy.isGlobalVCPolicy())
                            .isVPPolicy(policy.isVPPolicy())
                            .status(PolicyStatus.ACTIVE)
                            .build();
                    log.info("Registering a verification policy named: [{}]", policyEntity.getName());
                    return policyEntity;
                }).collect(Collectors.toList());

        policyRepository.saveAll(customPolicies);
    }

    @Transactional
    @Override
    public List<VerificationPolicyDto> findAllVerificationPolicies() {
        List<VerificationPolicyEntity> policyEntities = policyRepository.findAll();

        return policyEntities.stream()
                .map(policy -> modelMapper.map(policy, VerificationPolicyDto.class))
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public VerificationPolicyDto findVerificationPolicyByName(String name) {

        VerificationPolicyEntity policyEntity = policyRepository.findByName(name)
                .orElseThrow(() -> new ResourceNotFoundException("Policy", "Name", name));

        return modelMapper.map(policyEntity, VerificationPolicyDto.class);
    }


    @Transactional
    @Override
    public VerificationPolicyDto assignPolicyToVCTypes(String customPolicy, Set<String> vcTypes) {

        VerificationPolicyEntity policyEntity = policyRepository.findByName(customPolicy)
                .orElseThrow(() -> new ResourceNotFoundException("Policy", "Name", customPolicy));

        policyEntity.getAppliedVCs().addAll(vcTypes);
        VerificationPolicyEntity updatedPolicy = policyRepository.save(policyEntity);

        return modelMapper.map(updatedPolicy, VerificationPolicyDto.class);
    }
}
