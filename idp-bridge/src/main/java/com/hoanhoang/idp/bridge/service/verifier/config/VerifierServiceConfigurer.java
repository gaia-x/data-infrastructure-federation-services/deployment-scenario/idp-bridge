package com.hoanhoang.idp.bridge.service.verifier.config;


import id.walt.oid4vc.providers.CredentialVerifierConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class VerifierServiceConfigurer {

    @Bean
    public CredentialVerifierConfig credentialVerifierConfig(VerifierServiceConfig verifierServiceConfig, VerifierServiceBaseConfig verifierServiceBaseConfig){
        return new CredentialVerifierConfig(
                verifierServiceConfig.getClientId() != null ? verifierServiceConfig.getClientId() : verifierServiceBaseConfig.getBaseUrl() + "/openid4vp/verify",
                verifierServiceConfig.getClientIdScheme(),
                verifierServiceConfig.getRedirectUri() != null ? verifierServiceConfig.getRedirectUri() : verifierServiceBaseConfig.getBaseUrl() + "/redirectUri",
                verifierServiceConfig.getResponseUrl() != null ? verifierServiceConfig.getResponseUrl() : verifierServiceBaseConfig.getBaseUrl() + "/openid4vp/verify"
        );
    }

}
