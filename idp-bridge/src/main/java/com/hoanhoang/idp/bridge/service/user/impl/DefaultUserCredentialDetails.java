package com.hoanhoang.idp.bridge.service.user.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hoanhoang.idp.bridge.service.user.model.UserCredentialDetails;
import lombok.*;
import lombok.extern.slf4j.Slf4j;



/**
 * A default implementation of  UserCredentialDetails interface
 * @author van-hoan.hoang@eviden.com
 * @version 1.0
 * */
@Slf4j
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DefaultUserCredentialDetails implements UserCredentialDetails {

    @JsonIgnore
    private final String NAME = "SIMPLE-USER-CREDENTIAL-DETAIL";

    private String username;

    @Getter
    private String firstName;

    @Getter
    private String lastName;

    @Getter
    private String team;

    @Getter
    private String email;


    @Override
    public JsonNode loadCredentialSubject() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String simpleUser = objectMapper.writeValueAsString(this);
            return objectMapper.readTree(simpleUser);
        } catch (JsonProcessingException e) {
            log.error("Could not serialize user properties due to: {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String userCredentialDetailsImpl() {
        return NAME;
    }
}
