package com.hoanhoang.idp.bridge.service.wallet;

import com.hoanhoang.idp.bridge.entity.wallet.WalletEntity;
import com.hoanhoang.idp.bridge.exception.ResourceNotFoundException;
import com.hoanhoang.idp.bridge.repository.wallet.WalletRepository;
import com.hoanhoang.idp.bridge.service.wallet.model.WalletProfileDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class SSIWalletService implements WalletService {

    private final WalletRepository walletRepository;
    private final ModelMapper modelMapper;


    @Transactional
    @Override
    public WalletProfileDto createWalletProfile(WalletProfileDto profileDto) {
        log.debug("Creating a new wallet profile of provider: {}", profileDto.getProvider());

        WalletEntity wallet = modelMapper.map(profileDto, WalletEntity.class);
        WalletEntity createdWallet = walletRepository.save(wallet);

        log.debug("Successfully created !");
        return modelMapper.map(createdWallet, WalletProfileDto.class);
    }

    @Transactional(readOnly = true)
    @Override
    public List<WalletProfileDto> findAllWalletProfiles() {

        List<WalletEntity> walletEntities = walletRepository.findAll();

        return walletEntities.stream().map(walletEntity ->
                modelMapper.map(walletEntity, WalletProfileDto.class)).toList();
    }

    @Transactional(readOnly = true)
    @Override
    public WalletProfileDto findWalletProfileByIdOrProvider(Long walletId, String walletProvider) {
        WalletEntity wallet = walletRepository.findByIdOrProvider(walletId, walletProvider)
                .orElseThrow(() -> new ResourceNotFoundException("Wallet", "Id or Type", walletId + " " + walletProvider));

        return modelMapper.map(wallet, WalletProfileDto.class);
    }

    @Transactional
    @Override
    public void deleteWalletProfile(Long walletId, String walletProvider) {
        log.debug("Deleting a wallet with id: {} and type: {}", walletId, walletProvider);

        WalletEntity wallet = walletRepository.findByIdOrProvider(walletId, walletProvider)
                .orElseThrow(() -> new ResourceNotFoundException("Wallet", "Id or Type", walletId + " " + walletProvider));
        walletRepository.delete(wallet);

        log.debug("Successfully deleted !");
    }
}
