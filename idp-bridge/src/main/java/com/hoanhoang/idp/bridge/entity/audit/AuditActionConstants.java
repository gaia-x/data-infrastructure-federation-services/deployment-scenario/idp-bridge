package com.hoanhoang.idp.bridge.entity.audit;

import lombok.Getter;

@Getter
public enum AuditActionConstants {

    ADMIN_CREDENTIAL_SCHEMA_CREATE("admin_credential_schema_create"),
    ADMIN_CREDENTIAL_STATUS_LIST_CREATE("admin_credential_status_list_create"),
    ADMIN_USER_CREDENTIAL_STATUS_CHANGE("admin_user_credential_status_change"),


    USER_CREDENTIAL_REQUEST("user_credential_request"),
    USER_CREDENTIAL_STATUS_CHANGE_REQUEST("user_credential_status_change"),
    USER_CREDENTIAL_STATUS_CHANGE_CONFIRMATION("user_credential_status_change_confirmation"),
    USER_AUTHENTICATION_REQUEST("user_authentication_request");

    private final String action;

    AuditActionConstants(String action){
        this.action = action;
    }
}
