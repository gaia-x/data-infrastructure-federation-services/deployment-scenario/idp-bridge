package com.hoanhoang.idp.bridge.authorizationserver.config;

public interface Constants {

    String CLIENT_ENDPOINT_BASE_URL = "/oidc/clients";
    String INITIAL_TOKEN_ENDPOINT = "/token";
    String OIDC_SCOPE_OFFLINE_ACCESS = "offline_access";
    String OIDC_SCOPE_CREATE_CLIENT = "client.create";
    String OIDC_SCOPE_READ_CLIENT = "client.read";
    String VP_AUTHORIZATION_BASE_URL = "openid4vp://authorize";
}
