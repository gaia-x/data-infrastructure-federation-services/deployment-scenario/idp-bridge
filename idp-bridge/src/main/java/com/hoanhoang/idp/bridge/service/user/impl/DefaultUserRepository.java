package com.hoanhoang.idp.bridge.service.user.impl;


import com.fasterxml.jackson.databind.JsonNode;
import com.hoanhoang.idp.bridge.service.user.model.UserCredentialDetails;
import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
@Slf4j
public class DefaultUserRepository {

    @Setter
    @Getter
    private static List<UserCredentialDetails> users = new ArrayList<>();

    @PostConstruct
    private void initiateRepository(){
        if(users.size()  < 2){
            users.add(DefaultUserCredentialDetails.builder()
                            .email("hoanghoan.clc@gmail.com")
                            .firstName("Hoan")
                            .lastName("Hoang")
                            .username("HoanHoang")
                            .team("Innovation")
                    .build());

            users.add(DefaultUserCredentialDetails.builder()
                            .email("thanhhuyen.hsgs@gmail.com")
                            .firstName("Thanh")
                            .lastName("Huyen")
                            .username("ThanhHuyen")
                            .team("Innovation")
                    .build());
        }

        log.info("Current User repository has size of : {}", users.size());
    }

    public static UserCredentialDetails addUser(UserCredentialDetails userCredentialDetails){
        users.add(userCredentialDetails);

        return userCredentialDetails;
    }


    public static UserCredentialDetails findByUsername(String username){
        Optional<UserCredentialDetails> userCredentialDetails = users.stream()
                .filter(user -> user.getUsername().equals(username))
                .findFirst();

        if(userCredentialDetails.isEmpty()){
            log.error("Could not find user with username: {}", username);
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Could not find user with username: %s", username));
            return null;
        }
        return userCredentialDetails.get();
    }

    public static boolean deleteUserByUsername(String username){
        boolean isFulfilled = false;

        for(UserCredentialDetails userCredentialDetails : users){
            if(userCredentialDetails.getUsername().equals(username)){
                users.remove(userCredentialDetails);
                isFulfilled = true;
                break;
            }
        }

        return isFulfilled;
    }

}