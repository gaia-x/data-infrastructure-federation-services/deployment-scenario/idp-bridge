package com.hoanhoang.idp.bridge.service.vp;

import com.hoanhoang.idp.helper.JsonHelper;
import com.hoanhoang.idp.bridge.service.policy.custom.ExamplePolicy;
import com.hoanhoang.idp.bridge.service.verifier.model.CreateOID4VPSession;
import com.hoanhoang.idp.bridge.service.oidc.scope.ScopeService;
import com.hoanhoang.idp.bridge.service.policy.PresentationDefinitionPolicy;
import com.hoanhoang.idp.bridge.service.verifier.CredentialVerifierService;
import id.walt.credentials.verification.models.PolicyRequest;
import id.walt.credentials.verification.policies.JwtSignaturePolicy;
import id.walt.oid4vc.data.ResponseMode;
import id.walt.oid4vc.data.dif.*;
import id.walt.oid4vc.providers.PresentationSession;
import kotlinx.serialization.json.JsonElement;
import kotlinx.serialization.json.JsonObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class PresentationService implements PresentationManager {

    private final CredentialVerifierService ssiCredentialVerifierService;
    private final ScopeService scopeService;

    @Value("${aster.service.membership.credential.type}")
    private String asterXCredentialType;

    @Override
    public PresentationDefinition fromRequestedCredentials(Set<String> requestedCredentialTypes) {
        String name = null;
        String purpose = null;
        String asterXCredentialType = "vcard:Organization";
        String presentationID = "1";

        if(requestedCredentialTypes.contains(asterXCredentialType)) {
            name = "Aster-X Authentication";
            purpose = "Verifying Aster-X Membership";
            presentationID = UUID.randomUUID().toString();
        }

        List<InputDescriptor> inputDescriptors  = requestedCredentialTypes.stream()
                .map(type ->
                         new InputDescriptor(
                            type,
                            null,
                            null,
                            Map.of(
                                    !type.equals(asterXCredentialType) ? VCFormat.jwt_vc_json : VCFormat.ldp_vc,
                                    new VCFormatDefinition(
                                            Set.of("EdDSA", "RSA", "Ed25519"),
                                            null,
                                            new HashMap<String, JsonElement>())),
                            new InputDescriptorConstraints(
                                    List.of(
                                            new InputDescriptorField(
                                                    !type.equals(asterXCredentialType) ? List.of("$.type") : List.of("$['credentialSubject']['@type']"),
                                                    null,
                                                    null,
                                                    null,
                                                    new JsonObject(Map.of("type", JsonHelper.INSTANCE.createJSONPrimitive("string"), "pattern", JsonHelper.INSTANCE.createJSONPrimitive(type))),
                                                    null,
                                                    new HashMap<String, JsonElement>()
                                            )
                                    ),
                                    null,
                                    new HashMap<String, JsonElement>()
                            ),
                            null,
                            new HashMap<String, JsonElement>()
                    )
                ).collect(Collectors.toList());

            PresentationDefinition presentationDefinition = new PresentationDefinition(
                    presentationID,
                    inputDescriptors,
                    name,
                    purpose,
                    null,
                    null,
                    new HashMap<String, JsonElement>()

            );

        return presentationDefinition;
    }

    @Override
    public String createAuthorizationRequest(String authorizeBaseUrl,
                                             CreateOID4VPSession sessionRequest,
                                             ResponseMode responseMode,
                                             String successRedirectUri,
                                             String errorRedirectUri) {

        if((sessionRequest.getScopes() == null || sessionRequest.getScopes().isEmpty()) && (sessionRequest.getRequestCredentials() == null || sessionRequest.getRequestCredentials().isEmpty())){
            log.error("VP Session Request is invalid");
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, "VP Session Request is invalid");
        }

        if(sessionRequest.getScopes() != null & !sessionRequest.getScopes().isEmpty()){
            log.info("Creating a VP Session Request for the scopes: {}", Arrays.toString(sessionRequest.getScopes().toArray()));
            return createAuthorizationRequestFromRequestedScopes(authorizeBaseUrl, sessionRequest.getScopes(), responseMode, successRedirectUri, errorRedirectUri);
        }

        log.info("Creating a VP Session Request for the requested credentials: {}", Arrays.toString(sessionRequest.getRequestCredentials().toArray()));
        return createAuthorizationRequestFromRequestedCredentials(authorizeBaseUrl, sessionRequest.getRequestCredentials(), sessionRequest.getScopes(), responseMode, successRedirectUri, errorRedirectUri);
    }


    @Override
    public byte[] createAuthorizationRequestQRCodeFromRequestedCredentials(String authorizeBaseUrl,
                                                                           Set<String> requestedCredentials,
                                                                           Set<String> requestedScopes,
                                                                           ResponseMode responseMode,
                                                                           String successRedirectUri,
                                                                           String errorRedirectUri) {

        String authorizationRequest = createAuthorizationRequestFromRequestedCredentials(authorizeBaseUrl, requestedCredentials, requestedScopes, responseMode, successRedirectUri, errorRedirectUri);
        return QRCodeService.generateQRCodeImage(authorizationRequest);
    }


    @Override
    public String createAuthorizationRequestFromRequestedScopes(String authorizeBaseUrl,
                                                                Set<String> requestedScopes,
                                                                ResponseMode responseMode,
                                                                String successRedirectUri,
                                                                String errorRedirectUri) {

        Set<String> requestedCredentials = scopeService.toCredentialTypes(requestedScopes);
        log.info("Converting scopes {} to credentialTypes: {}", Arrays.toString(requestedScopes.toArray()), Arrays.toString(requestedCredentials.toArray()));
        return createAuthorizationRequestFromRequestedCredentials(authorizeBaseUrl, requestedCredentials, requestedScopes, responseMode, successRedirectUri, errorRedirectUri);
    }

    @Override
    public String createAuthorizationRequestFromRequestedCredentials(String authorizeBaseUrl,
                                                                     Set<String> requestedCredentials,
                                                                     Set<String> requestedScopes,
                                                                     ResponseMode responseMode,
                                                                     String successRedirectUri,
                                                                     String errorRedirectUri) {

        PresentationDefinition presentationDefinition = fromRequestedCredentials(requestedCredentials);
        log.info("Created a presentation definition: {} from credential types {}", presentationDefinition.toJSON(), Arrays.toString(requestedCredentials.toArray()));

        //Initiate a presentation session in the authorization server
        PresentationSession session = ssiCredentialVerifierService.initializeAuthorization(presentationDefinition, responseMode, requestedScopes, 180L);
        
        String sessionId = session.getId();
        String fullSuccessRedirectUri  = successRedirectUri + "/" + sessionId;
        log.info("Creating a full success redirect Uri:  {}", fullSuccessRedirectUri);

        //TODO: Extract all policies needs to be verified

        // Create a verification session with vp and vc policies
        ssiCredentialVerifierService.getSessionVerificationInfos().put(
                session.getId(),
                new CredentialVerifierService.SessionVerificationInformation(
                        List.of(new PolicyRequest(new JwtSignaturePolicy(), null), new PolicyRequest(new PresentationDefinitionPolicy(), null)),
                        List.of(new PolicyRequest(new JwtSignaturePolicy(), null), new PolicyRequest(new ExamplePolicy(), null)),  //Add example policy into VCs
                        new HashMap<>(),
                        fullSuccessRedirectUri,
                        errorRedirectUri
                )
        );

        Assert.notNull(session.getAuthorizationRequest(), "Authorization request should not be NULL");
        String vpRequest = authorizeBaseUrl +
                "?" +
                session.getAuthorizationRequest().toHttpQueryString();
        log.info("Generated a vp request for wallet authentication: {} ", vpRequest);

        return vpRequest;
    }

    @Override
    public boolean verify(Map<String, Object> vpToken) {
        return false;
    }

}
