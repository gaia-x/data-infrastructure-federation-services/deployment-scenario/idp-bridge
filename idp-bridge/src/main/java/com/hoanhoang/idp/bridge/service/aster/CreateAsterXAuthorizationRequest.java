package com.hoanhoang.idp.bridge.service.aster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateAsterXAuthorizationRequest {

    private Set<String> scopes;
}
