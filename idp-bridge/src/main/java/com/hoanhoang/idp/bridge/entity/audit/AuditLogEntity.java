package com.hoanhoang.idp.bridge.entity.audit;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(
        name = "ssi_audit_logs"
)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuditLogEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 9190005045656222499L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private AuditActionConstants action;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt;

    @Column(name = "target_resource", nullable = false)
    private String targetResource;

    @Column(nullable = false)
    private String details;
}
