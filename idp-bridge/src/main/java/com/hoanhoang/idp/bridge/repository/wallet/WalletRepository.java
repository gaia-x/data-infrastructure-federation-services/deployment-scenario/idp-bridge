package com.hoanhoang.idp.bridge.repository.wallet;

import com.hoanhoang.idp.bridge.entity.wallet.WalletEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.Optional;

@Repository
public interface WalletRepository extends JpaRepository<WalletEntity, Long> {

    Optional<WalletEntity> findByIdOrProvider(Long id, String provider);
}
