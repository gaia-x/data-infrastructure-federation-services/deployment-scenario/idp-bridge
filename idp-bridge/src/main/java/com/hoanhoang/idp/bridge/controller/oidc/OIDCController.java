package com.hoanhoang.idp.bridge.controller.oidc;


import com.hoanhoang.idp.bridge.controller.oidc.samples.OIDCSampleRequests;
import com.hoanhoang.idp.bridge.service.oidc.model.dto.ClientRegistrationDto;
import com.hoanhoang.idp.bridge.service.oidc.model.dto.CredentialMappingDto;
import com.hoanhoang.idp.bridge.service.oidc.model.dto.ScopeDto;
import com.hoanhoang.idp.bridge.service.oidc.model.request.ClientRegistration;
import com.hoanhoang.idp.bridge.service.oidc.model.request.ClientRegistrationUpdate;
import com.hoanhoang.idp.bridge.service.oidc.model.request.CredentialMappingRequest;
import com.hoanhoang.idp.bridge.service.oidc.model.response.ClientRegistrationResponse;
import com.hoanhoang.idp.bridge.service.oidc.model.request.ScopeRequest;
import com.hoanhoang.idp.bridge.service.oidc.model.response.CredentialMappingResponse;
import com.hoanhoang.idp.bridge.service.oidc.model.response.ScopeResponse;
import com.hoanhoang.idp.bridge.service.oidc.client.RegistrationService;
import com.hoanhoang.idp.bridge.service.oidc.scope.ScopeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Tag(
        name = "OIDC API",
        description = "Manage client registration, scope creation as well as credential mappings"
)
@RestController("idpOIDCController")
@RequestMapping("/oidc")
@RequiredArgsConstructor
@Slf4j
public class OIDCController {

    private final RegistrationService registrationService;
    private final ModelMapper modelMapper;
    private final ScopeService scopeService;

    @Operation(
            summary = "Creating a new OIDC Client",
            description = "Creating a new OIDC Client, i.e, **Keycloak**, **HR Frontend**, etc"  +
                    "\n\n An OIDC Client MUST use supported authentication methods when redirecting User to IDP Bridge"
    )

    @PostMapping(
            path = "/clients",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @io.swagger.v3.oas.annotations.parameters.RequestBody(
            content = @Content(
                    schema = @Schema(
                            implementation = ClientRegistrationDto.class
                    ),
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    examples = {
                            @ExampleObject(
                                    name = "Keycloak client",
                                    description = "Keycloak client",
                                    value = OIDCSampleRequests.keycloakClient
                            ),
                            @ExampleObject(
                                    name = "Client registration",
                                    description = "Client registration"
                            )
                    }
            )
    )
    public ResponseEntity<?> registerClient(@RequestBody @Valid ClientRegistration registrationRequest){
        ClientRegistrationDto clientRegistrationDto = modelMapper.map(registrationRequest, ClientRegistrationDto.class);
        ClientRegistrationResponse registrationResponse = registrationService.registerClient(clientRegistrationDto);
        return new ResponseEntity<>(registrationResponse, HttpStatus.OK);
    }

    @Operation(
            summary = "Get the details of an OIDC Client by ID",
            description = "Get the details of an OIDC Client by ID" +
                    "The details MUST specify Client's credential as well as supported authentication method"
    )
    @GetMapping(
            path = "/clients/{clientId}",
            produces = { MediaType.APPLICATION_JSON_VALUE }
    )
    public ResponseEntity<?> findClientById(@PathVariable("clientId") String clientId){
        ClientRegistrationResponse registrationResponse = registrationService.findRegisteredClientById(clientId);
        return new ResponseEntity<>(registrationResponse, HttpStatus.OK);
    }

    @Operation(
            summary = "Get details of all registered OIDC Clients",
            description = "Get details of all registered OIDC Clients"
    )
    @GetMapping(
            path = "/clients",
            produces = { MediaType.APPLICATION_JSON_VALUE }
    )
    public ResponseEntity<?> findAllClients(){
        List<ClientRegistrationResponse> clients = registrationService.findAllRegisteredClients();
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    @Operation(
            summary = "Update the details of an OIDC Client",
            description = "Update the details of an OIDC Client"
    )
    @PutMapping(
            path = "/clients/{clientName}",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<?> updateClient(@PathVariable("clientName") String clientName,
                                          @RequestBody ClientRegistrationUpdate clientRegistrationUpdate){

        ClientRegistrationDto clientRegistrationDto = modelMapper.map(clientRegistrationUpdate, ClientRegistrationDto.class);
        clientRegistrationDto.setClientName(clientName);

        ClientRegistrationResponse updatedClientResponse = registrationService.updateRegisteredClient(clientRegistrationDto);


        return new ResponseEntity<>(updatedClientResponse, HttpStatus.OK);
    }


    @Operation(
            summary = "Delete a registered client by clientName",
            description = "Delete a registered client by clientName"
    )
    @DeleteMapping(
            path = "/clients/{clientName}"
    )
    public ResponseEntity<?> deleteClient(@PathVariable("clientName")String clientName){
            registrationService.deleteRegisteredClient(clientName);
        return ResponseEntity.ok().build();
    }

    @Operation(
            summary = "Create a new supported scope for Verifier service",
            description = "Create a new supported scope for Verifier service"
    )
    @PostMapping(
            path = "/scopes",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )

    @io.swagger.v3.oas.annotations.parameters.RequestBody(
            content = @Content(
                    schema = @Schema(
                            implementation = ScopeRequest.class
                    ),
                    examples = {
                            @ExampleObject(
                                    name = "create employee scope",
                                    description = "create employee scope",
                                    value = OIDCSampleRequests.employeeScope
                            ),
                            @ExampleObject(
                                    name = "create bankId scope",
                                    description = "create bankId scope",
                                    value = OIDCSampleRequests.bankIdScope
                            ),
                            @ExampleObject(
                                    name = "create a new scope",
                                    description = "create a new scope"
                            )
                    }

            )
    )
    public ResponseEntity<?> createOIDCScope(@RequestBody @Valid ScopeRequest scopeRequest){
        ScopeDto scopeDto = modelMapper.map(scopeRequest, ScopeDto.class);
        ScopeDto createdScope = scopeService.addScope(scopeDto);
        ScopeResponse scopeResponse = modelMapper.map(createdScope, ScopeResponse.class);

        return new ResponseEntity<>(scopeResponse, HttpStatus.CREATED);
    }

    @Operation(
            summary = "Delete a scope and all associated credential mapping",
            description = "Delete associated credential mappings if mappingOnly is set to **true**" +
                    "\n \n Delete both specified scope and all credential mappings if mapping is set to **false**"
    )
    @DeleteMapping(
            path = "/scopes/{name}"
    )
    public ResponseEntity<?> removeOIDCScope(@PathVariable("name") String scopeName,
                                             @RequestParam(name = "mappingOnly", defaultValue = "true") boolean mappingOnly ){

        scopeService.removeScopeMappings(scopeName, mappingOnly);

        return ResponseEntity.ok().build();
    }

    @Operation(
            summary = "Get all credential mappings for a scope",
            description = "Get all credential mappings for a scope. " +
                    "\n\n Each credential mapping specifies the attributes in corresponding VCs that Wallet must present for authentication" +
                    "\n\n Attributes presented by Wallet are used to build IDToken returned to IDP Bridge's clients, i.e. *Keycloak*"
    )
    @GetMapping(
            path = "/scopes/{name}",
            produces = { MediaType.APPLICATION_JSON_VALUE }
    )
    public ResponseEntity<?> findScopeByName(@PathVariable("name") String scopeName){
        ScopeDto scopeDto = scopeService.findScope(scopeName);
        ScopeResponse scopeResponse = new ScopeResponse();
        scopeResponse.setName(scopeDto.getName());
        scopeResponse.setDescription(scopeDto.getDescription());

        scopeDto.getCredentialMappingDtos().forEach(credentialMappingDto -> {
                    CredentialMappingResponse response = modelMapper.map(credentialMappingDto, CredentialMappingResponse.class);
                    scopeResponse.getCredentialMappingResponses().add(response);
                });

        return new ResponseEntity<>(scopeResponse, HttpStatus.OK);
    }

    @Operation(
            summary = "Get all credential mappings for all scopes",
            description = "Get all credential mappings for all scopes" +
                    "\n\n Each credential mapping specifies the attributes in corresponding VCs that Wallet must present for authentication" +
                    "\n\n Attributes presented by Wallet are used to build IDToken returned to IDP Bridge's clients, i.e. *Keycloak*"
    )
    @GetMapping(
            path = "/scopes",
            produces = { MediaType.APPLICATION_JSON_VALUE }
    )
    public ResponseEntity<?> findAllScopes(){
        Set<ScopeDto> scopeDtos = scopeService.findAllScopes();
        Set<ScopeResponse> scopeResponses =  scopeDtos.stream().map(scopeDto -> {
            ScopeResponse scopeResponse = new ScopeResponse();
            scopeResponse.setName(scopeDto.getName());
            scopeResponse.setDescription(scopeDto.getDescription());
            for(CredentialMappingDto credentialMappingDto : scopeDto.getCredentialMappingDtos()){
                CredentialMappingResponse credentialMappingResponse = modelMapper.map(credentialMappingDto, CredentialMappingResponse.class);
                scopeResponse.getCredentialMappingResponses().add(credentialMappingResponse);
            }
            return scopeResponse;
        })
        .collect(Collectors.toSet());
        return new ResponseEntity<>(scopeResponses, HttpStatus.OK);
    }

    @Operation(
            summary = "Add a credential mapping for a scope",
            description = "Add a credential mapping for a scope" +
                    "\n\n Each credential mapping specifies the attributes in corresponding VCs that Wallet must present for authentication" +
                    "\n\n Attributes presented by Wallet are used to build IDToken returned to IDP Bridge's clients, i.e. *Keycloak*"
    )
    @PostMapping(
            path = "/scopes/mappings",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    @io.swagger.v3.oas.annotations.parameters.RequestBody(
            content = @Content(
                    schema = @Schema(
                            implementation = CredentialMappingRequest.CredentialMapping.class,
                            type = "array"
                    ),
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    examples = {
                            @ExampleObject(
                                    name = "SD credential mapping",
                                    description = "SD credential mapping",
                                    value = OIDCSampleRequests.sdJwtScopeMappings
                            ),
                            @ExampleObject(
                                    name = "JWT Employee Credential mapping",
                                    description = "Jwt Employee credential mapping",
                                    value = OIDCSampleRequests.jwtEmployeeScopeMappings
                            ),
                            @ExampleObject(
                                    name = "Jwt Bank ID Credential mapping",
                                    description = "Jwt BankId Credential mapping",
                                    value = OIDCSampleRequests.jwtBankIdScopeMappings
                            ),
                            @ExampleObject(
                                    name = "Credential mapping",
                                    description = "Credential mapping"
                            )
                    }
            )
    )
    public ResponseEntity<?> createCredentialMapping(@RequestParam(name = "scope") String scopeName,
                                                     @Valid @RequestBody Set<CredentialMappingRequest.CredentialMapping> mappingRequests){

        Set<CredentialMappingDto> credentialMappingDtos = new HashSet<>();
        mappingRequests.forEach(mappingRequest -> {
                    mappingRequest.verifyJsonPath();
                    mappingRequest.verifyCredentialFormat();
                    CredentialMappingDto credentialMappingDto = modelMapper.map(mappingRequest, CredentialMappingDto.class);
                    credentialMappingDtos.add(credentialMappingDto);
                });

        scopeService.addCredentialMapping(scopeName, credentialMappingDtos);

        return ResponseEntity.ok().build();

    }

}