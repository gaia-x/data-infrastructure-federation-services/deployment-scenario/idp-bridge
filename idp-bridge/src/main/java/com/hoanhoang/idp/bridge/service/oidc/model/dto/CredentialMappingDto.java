package com.hoanhoang.idp.bridge.service.oidc.model.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CredentialMappingDto {

    private String claim;

    private String claimPath;

    private String credentialFormat;

    private String credentialType;
}
