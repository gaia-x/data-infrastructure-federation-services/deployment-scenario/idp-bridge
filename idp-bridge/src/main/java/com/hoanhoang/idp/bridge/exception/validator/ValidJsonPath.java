package com.hoanhoang.idp.bridge.exception.validator;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = JsonPathValidator.class)
@Documented
public @interface ValidJsonPath {

    String message() default "Invalid JSONPath expression";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
