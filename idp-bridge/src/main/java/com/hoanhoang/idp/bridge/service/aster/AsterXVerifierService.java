package com.hoanhoang.idp.bridge.service.aster;


import com.hoanhoang.idp.helper.JsonHelper;
import id.walt.crypto.utils.JwsUtils;
import id.walt.oid4vc.data.dif.DescriptorMapping;
import id.walt.oid4vc.data.dif.InputDescriptor;
import id.walt.oid4vc.data.dif.PresentationDefinition;
import id.walt.oid4vc.data.dif.PresentationSubmission;
import id.walt.oid4vc.responses.TokenResponse;
import kotlinx.serialization.json.JsonArray;
import kotlinx.serialization.json.JsonElement;
import kotlinx.serialization.json.JsonObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class AsterXVerifierService {

//    private final AsterXPresentationService asterXPresentationService;

//    public AsterXVerifierService(){
//        this.asterXPresentationService = new AsterXPresentationService();
//        ClassPathResource resource = new ClassPathResource("service-matrix.properties");
//        log.info("Initializing Aster-X Verifier with service-config at: {}", resource.getPath());
//        new ServiceMatrix(resource.getPath());
////        this.asterXPresentationService.init(resource.getPath());
//    }

    // Return NULL if failed or subject_name if success
    public boolean verify(PresentationDefinition presentationDefinition, TokenResponse tokenResponse){

        // Verify signature & decode vpToken
        Assert.notNull(tokenResponse.getVpToken(), "Token Response should NOT be NULL");
        String vpToken = JsonHelper.INSTANCE.getContentFromJsonElement(tokenResponse.getVpToken());

        Assert.notNull(vpToken, "vp token should NOT be NULL");
        JwsUtils.JwsParts jwsParts =  JwsUtils.INSTANCE.decodeJws(vpToken, true);
        JsonObject payload = jwsParts.getPayload();
        List<String> vcs = new ArrayList<>();

        JsonObject vpObject = (JsonObject) payload.get("vp");
        Assert.notNull(vpObject, "vpObject should not be NULL");
        if(vpObject.get("verifiableCredential") instanceof JsonArray jsonVCs){
            for(JsonElement jsonVc: jsonVCs){
                String vc = JsonHelper.INSTANCE.getContentFromJsonElement(jsonVc);
                log.info("Extracting and Verifying  VC: {}", vc);
                verifyPresentation(vpToken, presentationDefinition, tokenResponse.getPresentationSubmission());
                doVerify(vc);
                vcs.add(vc);
            }
        } else{
            log.error("Failed to decode vcs inside the vp token: {}", vpToken);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Failed to decode vcs inside the vp token");
        }
        return true;
    }

    private void verifyPresentation(String vp, PresentationDefinition presentationDefinition, PresentationSubmission presentationSubmission){
        List<String> requiredCredentialTypes =  presentationDefinition.getInputDescriptors().stream()
                .map(InputDescriptor::getId)
                .toList();
        log.info("Required VC Types: {}", Arrays.toString(requiredCredentialTypes.toArray()));

        log.info("Presentation Definition is: {}", presentationDefinition.toString());
        log.info("Presentation Submission is: {}", presentationSubmission);

        // Get all input descriptor Ids
        String pdID = presentationDefinition.getId();
        Set<String> inputDescriptorIDs  = presentationDefinition.getInputDescriptors().stream()
                .map(InputDescriptor::getId)
                .collect(Collectors.toSet());

        // Get all presentation descriptors
        String psPdID = presentationSubmission.getDefinitionId();
        Set<String> descriptorIds = presentationSubmission.getDescriptorMap().stream()
                .map(DescriptorMapping::getId)
                .collect(Collectors.toSet());

        if(! pdID.equals(psPdID)) {
            log.error("Provided Definition Id: {} is not correct", pdID);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Provider definition ID is not correct");
        }

        if(! inputDescriptorIDs.containsAll(descriptorIds)){
            log.error("Missing some descriptors in the provided presentation submission");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Missing some descriptors in the provided presentation submission");
        }

        for (InputDescriptor inputDescriptor : presentationDefinition.getInputDescriptors()){
            for(DescriptorMapping descriptorMapping : presentationSubmission.getDescriptorMap()){
                if(inputDescriptor.getId().equals(descriptorMapping.getId())){
                    if(inputDescriptor.getFormat().get(descriptorMapping.getFormat()) == null){
                        log.error("Provided format: {} in the presentation submission is not supported", descriptorMapping.getFormat().toString());
                        throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                                String.format("Provided format: %s in the presentation submission is not supported", descriptorMapping.getFormat().toString()));

                    }
                }
            }
        }

    }
    private void doVerify(String vc){

        log.info("Verifying the validity of credential: {}", vc);

    }


}
