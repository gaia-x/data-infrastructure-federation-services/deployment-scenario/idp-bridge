package com.hoanhoang.idp.bridge.service.policy;

import com.hoanhoang.idp.bridge.service.policy.model.VerificationPolicyDto;

import java.util.List;
import java.util.Set;

public interface CredentialPolicyManager {

    List<VerificationPolicyDto> findAllVerificationPolicies();
    VerificationPolicyDto findVerificationPolicyByName(String name);
    VerificationPolicyDto assignPolicyToVCTypes(String customPolicy, Set<String> vcTypes);
}
