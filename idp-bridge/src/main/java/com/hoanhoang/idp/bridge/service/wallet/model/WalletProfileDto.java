package com.hoanhoang.idp.bridge.service.wallet.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class WalletProfileDto {

    private String provider;
    private String description;
    private Set<String> baseUrls = new HashSet<>();
    private Map<String, Object> customProperties = new HashMap<>();
}
