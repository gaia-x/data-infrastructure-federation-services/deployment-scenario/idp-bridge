package com.hoanhoang.idp.bridge.service.verifier.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateOID4VPSession {

    @JsonProperty("vp_policies")
    @Nullable
    private List<String> vpPolicies;

    @JsonProperty("vc_policies")
    @Nullable
    private List<String> vcPolicies;

    @JsonProperty("request_credentials")
    @Nullable
    private Set<String> requestCredentials = new HashSet<>();

    @JsonProperty("scopes")
    @Nullable
    private Set<String> scopes = new HashSet<>();

}
