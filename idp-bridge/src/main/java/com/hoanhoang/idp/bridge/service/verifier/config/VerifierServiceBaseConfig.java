package com.hoanhoang.idp.bridge.service.verifier.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "ssi.idp.service.verifier")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerifierServiceBaseConfig {

    private String baseUrl;
}
