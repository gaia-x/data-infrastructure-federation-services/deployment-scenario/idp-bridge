package com.hoanhoang.idp.bridge.authorizationserver.config;


import com.nimbusds.jose.Algorithm;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configuration.OAuth2AuthorizationServerConfiguration;


import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;
import java.util.UUID;

@Configuration
@Slf4j
@Getter
public class AuthorizationServerConfiguration {

    @Value("${ssi.idp.security.token.signing.key-size}")
    private int keySize;

    @Value("${ssi.idp.security.token.signing.algorithm}")
    private String keyAlgorithm;

    private RSAKey rsaKey;

    @Bean
    public JwtDecoder jwtDecoder(JWKSource<SecurityContext> jwkSource) {
        return OAuth2AuthorizationServerConfiguration.jwtDecoder(jwkSource);
    }

    @Bean
    public JWKSource<SecurityContext> jwkSource() throws NoSuchAlgorithmException {
        RSAKey rsaKey = generateRSA();
        this.rsaKey = rsaKey;
        JWKSet jwkSet = new JWKSet(rsaKey);
        return (jwkSelector, securityContext) -> jwkSelector.select(jwkSet);
    }

    private RSAKey generateRSA() throws NoSuchAlgorithmException {
        KeyPair keyPair = generateRsaKey();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

        return new RSAKey.Builder(publicKey)
                .privateKey(privateKey)
                .keyID(UUID.randomUUID().toString())
                .keyUse(KeyUse.SIGNATURE)
                .algorithm(Algorithm.parse("RS256"))
                .build();
    }


    private static String convertToPemFormat(Object key, boolean isPublicKey) {
        String header = isPublicKey ? "PUBLIC KEY" : "PRIVATE KEY";
        Base64.Encoder encoder = Base64.getMimeEncoder(64, "\n".getBytes());

        byte[] keyBytes = isPublicKey ? ((RSAPublicKey) key).getEncoded() : ((RSAPrivateKey) key).getEncoded();
        String keyPEM = encoder.encodeToString(keyBytes);

        return String.format("-----BEGIN %s-----\n%s\n-----END %s-----\n", header, keyPEM, header);
    }

    private KeyPair generateRsaKey() throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(keyAlgorithm);
        keyPairGenerator.initialize(keySize);
        return  keyPairGenerator.generateKeyPair();
    }


}