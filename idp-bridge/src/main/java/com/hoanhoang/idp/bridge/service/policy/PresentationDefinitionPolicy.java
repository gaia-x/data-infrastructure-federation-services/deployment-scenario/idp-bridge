package com.hoanhoang.idp.bridge.service.policy;

import com.hoanhoang.idp.helper.JavaCredentialWrapperValidatorPolicy;
import com.hoanhoang.idp.helper.JsonHelper;
import com.hoanhoang.idp.helper.VerificationResult;
import id.walt.crypto.utils.JwsUtils;
import id.walt.oid4vc.data.dif.InputDescriptor;
import id.walt.oid4vc.data.dif.PresentationDefinition;
import kotlinx.serialization.json.JsonArray;
import kotlinx.serialization.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


@Slf4j
@Component
public class PresentationDefinitionPolicy extends JavaCredentialWrapperValidatorPolicy {

    public PresentationDefinitionPolicy() {
        super(
                "presentation-definition",
                "Verifies that the presented VP contains all the required VCs",
                false,
                true
        );
    }

    @NotNull
    @Override
    public VerificationResult doVerify(@NotNull JsonObject data, @Nullable Object args, @NotNull Map<String, ?> context) {

        log.info("Verifying the vp token against the Presentation Definition Policy");
        //Get presentation_definition in the context
        PresentationDefinition pd = (PresentationDefinition) context.get("presentationDefinition");
        List<String> requiredCredentialTypes =  pd.getInputDescriptors().stream()
                .map(InputDescriptor::getId)
                .toList();
        log.info("Required VC Types: {}", Arrays.toString(requiredCredentialTypes.toArray()));


        JsonObject vp = (JsonObject)data.get("vp");
        Assert.notNull(vp, "VP should not be NULL");
        JsonArray vcs = (JsonArray)vp.get("verifiableCredential");

        Assert.notNull(vcs, "Array of credential should not be NULL");
        List<String> submittedVCTypes = vcs.stream().map(vc -> {
            String vcString  = JsonHelper.INSTANCE.getContentFromJsonElement(vc);
            Assert.notNull(vcString, "Verifiable Credential should not be NULL");
            JwsUtils.JwsParts parts = JwsUtils.INSTANCE.decodeJws(vcString, true);

            JsonObject payload = parts.getPayload();
            JsonObject vcData = (JsonObject) payload.get("vc");
            Assert.notNull(vcData, "Credential data should not be NULL");
            JsonArray types = (JsonArray) vcData.get("type");
            Assert.notNull(types, "Credential type should not be NULL");
            return JsonHelper.INSTANCE.getContentFromJsonElement(types.get(types.size() - 1));

        }).toList();

        log.info("Submitted VC Types: {}", Arrays.toString(submittedVCTypes.toArray()));

        if(new HashSet<>(submittedVCTypes).containsAll(requiredCredentialTypes))
            return new VerificationResult(true, "Success");

        return new VerificationResult(false, "User has not submitted all required VC Types");
    }

    @Nullable
    @Override
    public String getDescription() {
        return super.getDescription();
    }

    @NotNull
    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public boolean isGlobalVCPolicy() {
        return super.isGlobalVCPolicy();
    }

    @Override
    public boolean isVPPolicy() {
        return super.isVPPolicy();
    }
}
