package com.hoanhoang.idp.bridge.service.user.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hoanhoang.idp.bridge.exception.ResourceNotFoundException;
import com.hoanhoang.idp.bridge.service.user.base.UserCredentialDetailsProvider;
import com.hoanhoang.idp.bridge.service.user.model.UserCredentialDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * A default minimal user service provider
 * @version 1.0
 * */
@Service
@RequiredArgsConstructor
@Slf4j
public class DefaultUserCredentialDetailsProvider implements UserCredentialDetailsProvider {

    private final DefaultUserRepository userRepository;
    private final ObjectMapper objectMapper;

    @Override
    public UserCredentialDetails getUserCredentialDetails(String username) {
        UserCredentialDetails userCredentialDetails = DefaultUserRepository.findByUsername(username);
        if(userCredentialDetails == null){
            log.error("Could not found user with username: {}", username);
            throw new ResourceNotFoundException("User", "username", username);
        }

        return DefaultUserRepository.findByUsername(username);
    }

    @Override
    public void storeCredentialDetails(String username, String credentialTemplate, Map<String, Object> credential) {
        log.info("Mocking the credential storage in the default user service implementation");
        log.info("Stored credential data for username {} and template {}", username, credentialTemplate);
    }

    public UserCredentialDetails createUser(DefaultUserCredentialDetails userCredentialDetails){
        try {
            log.info("Creating a new user: {} in the system", objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(userCredentialDetails));
            if (DefaultUserRepository.findByUsername(userCredentialDetails.getUsername()) != null){
                log.error("There is already a user with username: {}", userCredentialDetails.getUsername());
                throw new ResponseStatusException(HttpStatus.CONFLICT, String.format("There is already a user with username: %s", userCredentialDetails.getUsername()));
            }
            log.info("Successfully created !");
            return DefaultUserRepository.addUser(userCredentialDetails);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public List<UserCredentialDetails> findAllUser(){
        return new ArrayList<>(DefaultUserRepository.getUsers());
    }

    public boolean deleteUser(String username){
        log.info("Deleting user with username: {}", username);

        return DefaultUserRepository.deleteUserByUsername(username);
    }
}
