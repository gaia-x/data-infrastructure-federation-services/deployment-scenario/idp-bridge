package com.hoanhoang.idp.bridge.controller.verifier;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hoanhoang.idp.bridge.authorizationserver.token.OAuthAsterTokenService;
import com.hoanhoang.idp.bridge.exception.ResourceNotFoundException;
import com.hoanhoang.idp.bridge.service.aster.*;
import com.hoanhoang.idp.bridge.service.verifier.model.CreateOID4VPSession;
import com.hoanhoang.idp.bridge.service.verifier.model.TokenResponseForm;
import com.hoanhoang.idp.bridge.service.verifier.CredentialVerifierService;
import com.hoanhoang.idp.bridge.service.vp.PresentationManager;
import com.hoanhoang.idp.bridge.service.vp.QRCodeService;
import com.hoanhoang.idp.helper.JsonHelper;
import id.walt.credentials.verification.models.PolicyResult;
import id.walt.credentials.verification.models.PresentationVerificationResponse;
import id.walt.oid4vc.data.ResponseMode;
import id.walt.oid4vc.providers.PresentationSession;
import id.walt.oid4vc.responses.TokenResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import kotlinx.serialization.json.Json;
import kotlinx.serialization.json.JsonElement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;


@Tag(
        name = "Credential Verifier API",
        description = "Manage inflow/outflow for OID4VP protocol"
)
//@RestController
@Controller("idpCredentialVerifierController")
@Slf4j
@RequestMapping("/openid4vp")
@RequiredArgsConstructor
public class CredentialVerifierController {

    private final PresentationManager presentationDefinitionManager;
    private final CredentialVerifierService credentialVerifierService;
    private final ObjectMapper objectMapper;
    private final OAuthAsterTokenService oAuthAsterTokenService;
    private final AsterXVpTokenService asterXVPTokenService;

    @Value("${ssi.idp.service.verifier.success-url}")
    private String successRedirectUri;

    @Value("${ssi.idp.service.verifier.failed-url}")
    private String errorRedirectUri;

    @Value("${ssi.idp.service.verifier.authorize-base-url}")
    private String authorizeBaseUrl;

    @Value("${ssi.wallet.base-url}")
    private String walletBaseUrl;

    @Value("${ssi.idp.service.verifier.response-mode}")
    private ResponseMode responseMode;

    @Value("${aster.service.verifier.path}")
    private String asterXVerificationPath;
    private final AuthenticationSuccessHandler successHandler = new SavedRequestAwareAuthenticationSuccessHandler();

    @Operation(
            summary = "Generate a QR Code for a new OID4VP session",
            description = "Generate a QR Code for a new OID4VP session"
    )
    @GetMapping(
            path = "/qr",
            produces = {MediaType.TEXT_HTML_VALUE}
    )
    public String createOID4VPSessionWithQRCode(@RequestParam(name = "scope", required = false) String scope, Model model){
        log.info("Creating an vp session authorization with scopes:  {}", scope);

        Set<String> requestedScopes = Arrays.stream(scope.split(" "))
                .collect(Collectors.toSet());

        // Default response mode
//        ResponseMode responseMode = ResponseMode.direct_post;

        CreateOID4VPSession sessionRequest = new CreateOID4VPSession();
        sessionRequest.setScopes(requestedScopes);
        log.info("Default response mode: {}", responseMode);
        String authorizationRequest = presentationDefinitionManager.createAuthorizationRequest(
                authorizeBaseUrl,
                sessionRequest,
                responseMode,
                successRedirectUri,
                errorRedirectUri);

        byte[] qrCode = QRCodeService.generateQRCodeImage(authorizationRequest);
        String qrCodeBase64 = Base64.getEncoder().encodeToString(qrCode);
        String sessionId = authorizationRequest.substring(authorizationRequest.lastIndexOf("%2F") + 3);

        log.info("Created a new authorization session with id: [{}]", sessionId);
        model.addAttribute("sessionId", sessionId);
        model.addAttribute("qrCodeText", authorizationRequest);
        model.addAttribute("qrCodeImage", qrCodeBase64);

        String params = authorizationRequest.substring(authorizationRequest.indexOf("?"));

        String walletRedirectUrl = walletBaseUrl + "api/siop/initiatePresentation" + params;


        log.info("Wallet Redirect Url: {}", walletRedirectUrl);
        model.addAttribute("walletRedirectUrl", walletRedirectUrl);


        log.info("Waiting for user to submit requested credentials");

        return "oid4vp-qr";
    }

    @Operation(
            summary = "Create a new OID4VP session",
            description = "This is an API-initiated endpoint, which is for demo purpose to manually generate a verifiable presentation request"
    )
    @PostMapping(
            path = "/verify",
            consumes = {MediaType.APPLICATION_JSON_VALUE}
    )
    @ResponseBody
    public ResponseEntity<?> createOID4VPSession(@RequestHeader(name = "authorizeBaseUrl", required = false, defaultValue = "openid4vp://authorize") String authorizeBaseUrl,
                                                 @RequestHeader(name = "responseMode", required = false, defaultValue = "direct_post") ResponseMode responseMode,
                                                 @RequestHeader(name = "successRedirectUri", required = false, defaultValue = "http://localhost:3000/success") String successRedirectUri,
                                                 @RequestHeader(name = "errorRedirectUri", required = false, defaultValue = "http://localhost:3000/failed") String errorRedirectUri,
                                                 @RequestBody @Valid CreateOID4VPSession createOID4VPSession) throws JsonProcessingException {

        log.info("Creating a new vp authorization session with the request [{}]", objectMapper.writeValueAsString(createOID4VPSession));
        String vpRequest = presentationDefinitionManager.createAuthorizationRequestFromRequestedCredentials(authorizeBaseUrl, createOID4VPSession.getRequestCredentials(), Set.of(), responseMode, successRedirectUri, errorRedirectUri);

        return new ResponseEntity<>(vpRequest, HttpStatus.OK);
    }

    @Operation(
            summary = "Verify the state of an authentication session",
            description = "verify the state of an authentication session"
    )
    @GetMapping(
            path = "/session/{state}"
    )
    @ResponseBody
    public ResponseEntity<?> verifySessionInfo(@PathVariable("state") String sessionId){
        log.info("Verifying the current state of an initiated vp session with id: [{}]", sessionId);

        PresentationSession session = credentialVerifierService.getSession(sessionId);
        if(session == null){
            log.error("Could not find a session associated to the provided sessionId {}", sessionId);
            throw new ResourceNotFoundException("Session", "sessionId", sessionId);
        }
        PresentationVerificationResponse response = credentialVerifierService.getPolicyResults().get(session.getId());
        if(response == null)
            return ResponseEntity.ok(false);

        boolean overallResult = response.overallSuccess();
        log.info("The overall verification result for the Session [{}] is: [{}]", sessionId, overallResult);

        if(!overallResult){
            log.info("The following policies are failed: ");
            List<List<PolicyResult>> failedPolicyResults = response.getResults().stream()
                    .map(resultEntry -> {
                        List<PolicyResult> resultForACredential = resultEntry.getPolicyResults();
                        return resultForACredential.stream()
                                .filter(policyResult -> {
                                    log.info("Policy Result for {} is {}", policyResult.getRequest().getPolicy().getName(), policyResult.isSuccess());
                                    return ! policyResult.isSuccess();
                                })
                                .collect(Collectors.toList());
                    })
                    .toList();

            StringJoiner joiner = new StringJoiner(", ");
            failedPolicyResults.stream()
                    .flatMap(List::stream)
                    .map(policyResult -> policyResult.getRequest().getPolicy().getName())
                    .forEach(joiner::add);
            String combinedFailedPolicyNames = joiner.toString();
            log.warn("The following policies are failed: {}", combinedFailedPolicyNames);
        }


        return ResponseEntity.ok(overallResult);
    }

//    @Operation(
//            summary = "Issue access token"
//    )
//    @PostMapping(
//            path = "/aster-x/issue"
//    )
//    public ResponseEntity<?> issueAccessToken(){
//        return new ResponseEntity<>(oAuthAsterTokenService.issue(), HttpStatus.OK);
//    }



    @Operation(
            summary = "Issuing a vp token from provided credentials",
            description = "Issuing a vp token from provided credentials and two following mandatory parameters:" +
                    "\n\n issuer: is assigned to *iss* claim" +
                    "\n\n subject: is assigned to *sub* claim"
    )
    @PostMapping("/aster-x/vp/issue")
    public ResponseEntity<?> issueVPToken(@RequestBody AsterXVpTokenRequest tokenRequest){

        String vpToken = asterXVPTokenService.issueVPToken(tokenRequest.getIssuer(), tokenRequest.getSubject(), tokenRequest.getVcs());
        log.info("Issued a vpToken: {} for subject: {}", vpToken, tokenRequest.getSubject());

        return new ResponseEntity<>(Map.of("vp_token", vpToken), HttpStatus.OK);

    }

    @Operation(
            summary = "Initializing an aster-x authorization request",
            description = "Initializing an aster-x authorization request by proving one or a set of pre-configured scopes"
    )
    @PostMapping(
            path = "/aster-x/verify"
    )
    public ResponseEntity<?> createAsterXOID4VPSession(@RequestHeader(name = "authorizeBaseUrl", required = false, defaultValue = "openid4vp://authorize") String authorizeBaseUrl,
                                                       @RequestHeader(name = "responseMode", required = false, defaultValue = "direct_post") ResponseMode responseMode,
                                                       @RequestBody @Valid CreateAsterXAuthorizationRequest asterAuthorizationRequest){
        log.info("Initializing an aster-x oid4vp session with scopes: {}", asterAuthorizationRequest.getScopes());
        Set<String> refinedRequestScopes = new HashSet<>(asterAuthorizationRequest.getScopes());
        refinedRequestScopes.remove("openid");

        if(asterAuthorizationRequest.getScopes().isEmpty() || refinedRequestScopes.isEmpty()){
            log.error("The provided Aster X Authorization Request lacks supported scopes");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The provided Aster X Authorization Request lacks supported scopes");
        }

        CreateOID4VPSession createOID4VPSession = CreateOID4VPSession.builder()
                .requestCredentials(Set.of())
                .vcPolicies(List.of())
                .vpPolicies(List.of())
                .scopes(asterAuthorizationRequest.getScopes())
                .build();

        String authzRequest = presentationDefinitionManager.createAuthorizationRequest(authorizeBaseUrl, createOID4VPSession, responseMode, successRedirectUri, errorRedirectUri);

        // Decode and re-encode url
        String decodedAuthzRequest = URLDecoder.decode(authzRequest, StandardCharsets.UTF_8);
        log.info("BASE:  {}", decodedAuthzRequest);
        ///openid4vp/aster-x/verify
        if(decodedAuthzRequest.contains("/openid4vp/verify")){
            decodedAuthzRequest = decodedAuthzRequest.replace("/openid4vp/verify", asterXVerificationPath);
            authzRequest = URLEncoder.encode( decodedAuthzRequest, StandardCharsets.UTF_8);
        }


        return new ResponseEntity<>(new AsterXAuthorizationRequest(authzRequest), HttpStatus.OK);
    }

    @Operation(
            summary = "Verify an Aster-x authorization response",
            description = "Verify an Aster-x authorization response"
    )
    @PostMapping(
            path = "/aster-x/verify/{state}",
            consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE }
    )
    public ResponseEntity<?> verifyAsterVPResponseByState(@PathVariable("state") String state,
                                                          @RequestBody TokenResponseForm tokenResponseForm) throws JsonProcessingException {


        PresentationSession session = credentialVerifierService.getPresentationSessions().get(state);

        if(session == null){
            log.error("Could not find Session for given ID: [{}]", state);
            throw new ResourceNotFoundException("Session", "State", state);
        }

        if(tokenResponseForm == null){
            log.error("Token Response Form is not provided");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Token response is not provided");
        }

        String vpToken = Json.Default.encodeToString(JsonElement.Companion.serializer(), tokenResponseForm.getVpToken());
        String presentationSubmission = objectMapper.writeValueAsString(tokenResponseForm.getPresentationSubmissionForm());

        log.info("Presentation submission:  {} & vp_token: {}", presentationSubmission, vpToken);

        TokenResponse tokenResponse = TokenResponse.Companion.fromHttpParameters(Map.of("vp_token", List.of(vpToken), "presentation_submission", List.of(presentationSubmission)));

        CredentialVerifierService.SessionVerificationInformation sessionVerificationInfo = credentialVerifierService.getSessionVerificationInfos().get(session.getId());
        if(sessionVerificationInfo == null){
            log.error("Could not find SessionInfo for the sessionId: [{}]", state);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Could not find session information for the specified state:  %s", state));
        }

        PresentationSession verifiedSession = credentialVerifierService.verifyAsterXPresentation(tokenResponse, session);
        if(Boolean.FALSE.equals(verifiedSession.getVerificationResult())){
            // Listing all failed policy
            log.error("Failed to verify the provided vp_token");
            return new ResponseEntity<>(Map.of("details", "Failed to verify the provided vp token, please try again"), HttpStatus.BAD_REQUEST);
        } else {
            // Return an access token from Token Context
            log.info("Successfully authenticate client via an aster-x OID4VP session. Start issuing an access token");
//            oAuthAsterTokenService.
            assert verifiedSession.getCustomParameters() != null;
            Assert.notNull(verifiedSession.getCustomParameters(), "Custom Parameters should not be NULL");

            String subjectName = (String)verifiedSession.getCustomParameters().get("username");
            log.info("Issuing an access token for user: {}", subjectName);
            Set<String> scopes = (Set<String>) verifiedSession.getCustomParameters().get("scopes");

            String accessToken = oAuthAsterTokenService.issue(subjectName, scopes);
            return new ResponseEntity<>(new AsterXAuthorizationResponse(accessToken), HttpStatus.OK);
        }

    }

    @Operation(
            summary = "Get a Presentation Definition by id",
            description = "Get a Presentation Definition by id"
    )
    @GetMapping(
            path = "/pd/{id}"
    )
    @ResponseBody
    public ResponseEntity<?> getPresentationDefinition(@PathVariable("id") String pdId){
        log.info("Presentation Definition Uri is not yet supported for the ID:  [{}]", pdId);
        return ResponseEntity.ok(String.format("Presentation Definition Uri is not yet supported for the ID: %s", pdId));
    }

    @Operation(
            summary = "Verify the state of an authentication session",
            description = "Verify the state of an authentication session"
    )
    @PostMapping(
            path = "/verify/{state}",
            consumes = { MediaType.APPLICATION_FORM_URLENCODED_VALUE }
    )
    @ResponseBody
    public ResponseEntity<?> verifyVPResponseByState(@PathVariable("state") String state,
                                                     @RequestBody TokenResponseForm tokenResponseForm) throws JsonProcessingException {

        log.info("Verifying the response token submitted from Wallet for Session: [{}]", state);

        PresentationSession session = credentialVerifierService.getPresentationSessions().get(state);

        if(session == null){
            log.error("Could not find Session for given ID: [{}]", state);
            throw new ResourceNotFoundException("Session", "State", state);
        }

        if(tokenResponseForm == null){
            log.error("Token Response Form is not provided");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Token response is not provided");
        }

        String vpToken = Json.Default.encodeToString(JsonElement.Companion.serializer(), tokenResponseForm.getVpToken());
        String presentationSubmission = objectMapper.writeValueAsString(tokenResponseForm.getPresentationSubmissionForm());

        log.info("Presentation submission:  {} & vp_token: {}", presentationSubmission, vpToken);

        TokenResponse tokenResponse = TokenResponse.Companion.fromHttpParameters(Map.of("vp_token", List.of(vpToken), "presentation_submission", List.of(presentationSubmission)));

        CredentialVerifierService.SessionVerificationInformation sessionVerificationInfo = credentialVerifierService.getSessionVerificationInfos().get(session.getId());

        if(sessionVerificationInfo == null){
            log.error("Could not find SessionInfo for the sessionId: [{}]", state);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Could not find session information for the specified state:  %s", state));
        }

        PresentationSession presentationSession = credentialVerifierService.verifyPresentation(tokenResponse, session);

        if(Boolean.TRUE.equals(presentationSession.getVerificationResult())){
            // Redirect wallet to the successUrl
            String redirectUri  = sessionVerificationInfo.getSuccessRedirectUri();
            log.info("Sending wallet the redirectUri: {}", redirectUri);

            //-----------------------------------DEBUG
            PresentationVerificationResponse verificationResponse = credentialVerifierService.getPolicyResults().get(session.getId());
            List<String> policyNames = new ArrayList<>();
            verificationResponse.getResults().stream()
                    .flatMap(entry -> entry.getPolicyResults().stream())
                    .forEach(result -> policyNames.add(result.getRequest().getPolicy().getName()));

            log.info("Successfully verified policies are: {}", Arrays.toString(policyNames.toArray()));

            return new ResponseEntity<>(redirectUri, HttpStatus.OK);
        } else{
            PresentationVerificationResponse verificationResponse = credentialVerifierService.getPolicyResults().get(session.getId());
            String redirectUri = sessionVerificationInfo.getErrorRedirectUri();
            if(redirectUri != null){
                // Redirect user to failedUrl if exists
                return new ResponseEntity<>(redirectUri, HttpStatus.OK);
            } else {
                if(verificationResponse == null){
                    return new ResponseEntity<>("Failed to verify VP Token", HttpStatus.BAD_REQUEST);
                } else {
                    //Return to Wallet a list of failed Policy
                    List<List<PolicyResult>> failedPolicyResults = verificationResponse.getResults().stream()
                            .map(resultEntry -> {
                                List<PolicyResult> resultForACredential = resultEntry.getPolicyResults();
                                return resultForACredential.stream()
                                        .filter(policyResult -> ! policyResult.isSuccess())
                                        .collect(Collectors.toList());
                            })
                            .toList();

                    StringJoiner joiner = new StringJoiner(", ");
                    failedPolicyResults.stream()
                            .flatMap(List::stream)
                            .map(policyResult -> policyResult.getRequest().getPolicy().getName())
                            .forEach(joiner::add);
                    String combinedFailedPolicyNames = joiner.toString();
                    log.warn("The following policies are failed: {}", combinedFailedPolicyNames);

                    return new ResponseEntity<>(combinedFailedPolicyNames, HttpStatus.BAD_REQUEST);
                }
            }
        }
    }

    @Operation(
            summary = "Continue the OIDC Flow once User has been authenticated via out-of-band channel",
            description = "Continue the OIDC Flow once user has been authenticated via out-of-band channel"
    )
    @GetMapping(
            path = "/continue/{sessionId}"
    )
    public void continueOpenIDFlow(HttpServletRequest request,
                                   HttpServletResponse response,
                                   @PathVariable("sessionId") String sessionId){

        //Check if session is authenticated
        PresentationSession session = credentialVerifierService.getSession(sessionId);
        if(session == null){
            log.error("Could not find a session associated to the provided sessionId {}", sessionId);
            throw new ResourceNotFoundException("Session", "sessionId", sessionId);
        }

        PresentationVerificationResponse verificationResponse = credentialVerifierService.getPolicyResults().get(session.getId());
        if(verificationResponse == null || !verificationResponse.overallSuccess()){
            log.error("The verification is not found or not complete for session with Id: {}", sessionId);
        } else {
            log.info("The verification for the session [{}] is completed", sessionId);

            // Decode the VP to extract the "iss" claim which is then  put into the "sub" claim of ID Token
            String username = (String) Objects.requireNonNull(credentialVerifierService.getPresentationSessions()
                            .get(sessionId).getCustomParameters())
                    .get("username");

            log.info("User {} has successfully authenticated via an OID4VP session", username);

            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(username, "N/A", List.of(new SimpleGrantedAuthority("USER")));

            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(authentication);
            HttpSession httpSession = request.getSession(true);
            httpSession.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext);

            try {
                successHandler.onAuthenticationSuccess(request, response, authentication);
            } catch (IOException | ServletException e) {
                log.error("Could not continue the OIDC Flow due to: {}", e.getMessage());
                throw new RuntimeException(e);
            }
        }

    }

}
