package com.hoanhoang.idp.bridge.entity.verifier;


import com.hoanhoang.idp.bridge.entity.enums.PolicyStatus;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ssi_default_verification_policies")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VerificationPolicyEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 5118028381003407119L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private boolean isGlobalVCPolicy;

    @Column(nullable = false)
    private boolean isVPPolicy;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PolicyStatus status;

    @Column(nullable = false)
    @ElementCollection
    @CollectionTable(name = "ssi_verification_policies_applied_credentials")
    private Set<String> appliedVCs = new HashSet<>();

}
