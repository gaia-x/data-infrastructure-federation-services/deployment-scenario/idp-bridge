package com.hoanhoang.idp.bridge.controller.oidc.samples;

public class OIDCSampleRequests {

    public static final String sdJwtScopeMappings = """
            [
              {
                "claim": "country",
                "claimPath": "$.credentialSubject.country",
                "credentialType": "AuthenticationCredential",
                "credentialFormat": "sd_jwt_vc"
              },
              {
                "claim": "given_name",
                "claimPath": "$.credentialSubject.firstName",
                "credentialType": "AuthenticationCredential",
                "credentialFormat": "sd_jwt_vc"
              },
              {
                "claim": "family_name",
                "claimPath": "$.credentialSubject.lastName",
                "credentialType": "AuthenticationCredential",
                "credentialFormat": "sd_jwt_vc"
              }
            ]
            """;

    public static final String jwtEmployeeScopeMappings = """
                [
                  {
                    "claim": "family_name",
                    "claimPath": "$.vc.credentialSubject.lastName",
                    "credentialType": "EmployeeCredential",
                    "credentialFormat": "jwt_vc_json"
                  },
                  {
                    "claim": "given_name",
                    "claimPath": "$.vc.credentialSubject.firstName",
                    "credentialType": "EmployeeCredential",
                    "credentialFormat": "jwt_vc_json"
                  },
                  {
                    "claim": "vc_type",
                    "claimPath": "$.vc.type[1]",
                    "credentialType": "EmployeeCredential",
                    "credentialFormat": "jwt_vc_json"
                  }
                ]
            """;

    public static final String jwtBankIdScopeMappings = """
                [
                  {
                    "claim": "family_name",
                    "claimPath": "$.vc.credentialSubject.familyName",
                    "credentialType": "BankId",
                    "credentialFormat": "jwt_vc_json"
                  },
                  {
                    "claim": "given_name",
                    "claimPath": "$.vc.credentialSubject.givenName",
                    "credentialType": "BankId",
                    "credentialFormat": "jwt_vc_json"
                  }
                ]
            """;

    public static final String keycloakClient = """
            {
              "client_name": "keycloak",
              "redirect_uris": [
                "http://keycloak:8081/realms/master/broker/oidc/endpoint"
              ],
              "scopes": [
                "openid", "eviden", "auth", "employee", "mdoc", "bankId"
              ]
            }
            """;

    public static final String employeeScope = """
            {
                "name": "employee",
                "description": "authenticate user via EmployeeCredential"
            }
            """;
    public static final String bankIdScope = """
            {
                "name": "bankId",
                "description": "authenticate user via EmployeeCredential"
            }
            """;
}
