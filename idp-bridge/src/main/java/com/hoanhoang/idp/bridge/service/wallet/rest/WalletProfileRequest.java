package com.hoanhoang.idp.bridge.service.wallet.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class WalletProfileRequest {

    @JsonProperty("provider")
    private String provider;

    @JsonProperty("description")
    private String description;

    @JsonProperty("base_urls")
    private Set<String> baseUrls = new HashSet<>();

    @JsonProperty("customProperties")
    private Map<String, Object> customProperties = new HashMap<>();
}
