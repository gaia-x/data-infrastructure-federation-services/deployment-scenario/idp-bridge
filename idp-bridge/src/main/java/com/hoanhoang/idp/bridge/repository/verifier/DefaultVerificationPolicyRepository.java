package com.hoanhoang.idp.bridge.repository.verifier;

import com.hoanhoang.idp.bridge.entity.verifier.VerificationPolicyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface DefaultVerificationPolicyRepository extends JpaRepository<VerificationPolicyEntity, Long> {

    boolean existsByName(String name);
    Optional<VerificationPolicyEntity> findByName(String name);
}
