package com.hoanhoang.idp.bridge.service.oidc.scope;

import com.hoanhoang.idp.bridge.service.oidc.model.dto.CredentialMappingDto;
import com.hoanhoang.idp.bridge.service.oidc.model.dto.ScopeDto;

import java.util.Set;

public interface ScopeService {

    ScopeDto addScope(ScopeDto scopeDto);
    void removeScopeMappings(String scopeName, boolean mappingOnly);
    ScopeDto findScope(String scopeName);
    Set<String> toCredentialTypes(Set<String> scopes);
    Set<String> fromCredentialTypes(Set<String> credentialTypes);

    void addCredentialMapping(String scopeName, Set<CredentialMappingDto> credentialMappingDtos);
    void removeCredentialMapping(String scopeName, Set<Long> mappingIds);
    Set<ScopeDto> findAllScopes();
}
