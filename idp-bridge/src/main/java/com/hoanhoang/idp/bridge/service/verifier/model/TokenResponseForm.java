package com.hoanhoang.idp.bridge.service.verifier.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import kotlinx.serialization.json.JsonElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenResponseForm {

    @JsonProperty("vp_token")
    @NotNull
    private JsonElement vpToken;

    @JsonProperty("presentation_submission")
    private PresentationSubmissionForm presentationSubmissionForm;
}
