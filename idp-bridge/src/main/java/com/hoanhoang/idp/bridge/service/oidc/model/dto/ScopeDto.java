package com.hoanhoang.idp.bridge.service.oidc.model.dto;


import com.hoanhoang.idp.bridge.service.oidc.model.dto.CredentialMappingDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScopeDto {

    private String name;

    private String description;

    private Set<CredentialMappingDto> credentialMappingDtos = new HashSet<>();
}
