package com.hoanhoang.idp.bridge.service.oidc.model.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.hoanhoang.idp.bridge.exception.validator.ValidJsonPath;
import com.jayway.jsonpath.JsonPath;
import id.walt.oid4vc.data.dif.VCFormat;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CredentialMappingRequest {


    @JsonProperty("scope")
    @NotNull
    private String scope;

    @JsonProperty("mappings")
    private Set<CredentialMappingRequest> mappingRequests;


    @Slf4j
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CredentialMapping {

        @NotNull
        private String claim;

        @ValidJsonPath
        private String claimPath;

        @NotNull
        private String credentialType;

        @NotNull
        private String credentialFormat = VCFormat.jwt_vc.toString(); //Default


        public void verifyJsonPath(){
            try{
                JsonPath.compile(this.claimPath);
            } catch(Exception e){
                log.error("Provided JsonPath: {} is not correct", this.claimPath);
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Provided JsonPath %s is not correct", this.claimPath));
            }
        }
        public void verifyCredentialFormat(){
            try{
                VCFormat.valueOf(this.credentialFormat);
            } catch (Exception e){
                log.error("Provided vc format :{} is not correct or supported", this.credentialFormat);
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Provided vc format: %s is not correct or supported", this.credentialFormat));
            }
        }
    }
}
