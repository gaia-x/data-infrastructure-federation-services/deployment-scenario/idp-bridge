package com.hoanhoang.idp.bridge.service.user.base;

import com.hoanhoang.idp.bridge.service.user.model.UserCredentialDetails;

import java.util.Map;

public interface UserCredentialDetailsProvider {

    UserCredentialDetails getUserCredentialDetails(String username);
    void storeCredentialDetails(String username, String credentialTemplate, Map<String, Object> credential);
}
