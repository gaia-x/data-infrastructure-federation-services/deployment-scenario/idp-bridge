package com.hoanhoang.idp.bridge.service.oidc.model.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IATClientRegistrationResponse {

    @JsonProperty("registration_access_token")
    private String registrationAccessToken;

    @JsonProperty("registration_client_uri")
    private String registrationClientUri;

    @JsonProperty("client_name")
    private String clientName;

    @JsonProperty("client_id")
    private String clientId;

    @JsonProperty("client_secret")
    private String clientSecret;

    @JsonProperty("redirect_uris")
    private List<String> redirectUris;

    @JsonProperty("logo_uri")
    private String logoUri;

    @JsonProperty("contact")
    private List<String> contact;

    @JsonProperty("scope")
    private String scope;

    @JsonProperty("client_authentication_methods")
    private String clientAuthenticationMethods;

}
