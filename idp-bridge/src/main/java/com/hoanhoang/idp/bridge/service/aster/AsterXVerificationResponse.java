package com.hoanhoang.idp.bridge.service.aster;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class AsterXVerificationResponse {

    private boolean overallSuccess;
    private String details;


}
