package com.hoanhoang.idp.bridge.authorizationserver.token;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.core.oidc.endpoint.OidcParameterNames;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.server.authorization.token.JwtEncodingContext;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenCustomizer;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * OAuthTokenCustomizerConfiguration
 * Enable customization of issued tokens based on contexts and authentication methods
 * In case of OID4VP/SIOPv2: issued tokens can contain attributes in the submitted credentials
 * In case of identity federation: issued tokens can contain information given by external IDP
 */
@Configuration
@Slf4j
@RequiredArgsConstructor
public class OAuthTokenCustomizerConfiguration {

    private final OAuthUserInfoMapperService userInfoMapperService;
    private static final Set<String> ID_TOKEN_CLAIMS = Set.of(
            IdTokenClaimNames.ISS,
            IdTokenClaimNames.SUB,
            IdTokenClaimNames.AUD,
            IdTokenClaimNames.EXP,
            IdTokenClaimNames.IAT,
            IdTokenClaimNames.AUTH_TIME,
            IdTokenClaimNames.NONCE,
            IdTokenClaimNames.ACR,
            IdTokenClaimNames.AMR,
            IdTokenClaimNames.AZP,
            IdTokenClaimNames.AT_HASH,
            IdTokenClaimNames.C_HASH
    );


    @Bean
    public OAuth2TokenCustomizer<JwtEncodingContext> tokenCustomizer(OAuthAuthenticatedUserInfoContext oauthTokenInfoManager) {

        return context -> {
            Authentication authentication = context.getPrincipal();

            log.info("Customizing user info for OID4VP session");
            if (OidcParameterNames.ID_TOKEN.equals(context.getTokenType().getValue())) {
                String username = context.getPrincipal().getName();
                OidcUserInfo userInfo = oauthTokenInfoManager.loadUserSessionInfo(context.getPrincipal().getName());

                // Adding user claims into userInfoMapperService for customizing userinfo response
                userInfoMapperService.addClaimMappingsForUser(userInfo.getClaim("sub"), userInfo.getClaims());

                log.info("Issuing a ID Token for user: {} with claims: {}", username, userInfo.getClaims());
                context.getClaims().claims(claims -> claims.putAll(userInfo.getClaims()));
            }

        };
    }

    private Map<String, Object> extractClaims(Authentication principal) {
        Map<String, Object> claims;
        if (principal.getPrincipal() instanceof OidcUser oidcUser) {
            OidcIdToken idToken = oidcUser.getIdToken();
            claims = idToken.getClaims();
        } else if (principal.getPrincipal() instanceof OAuth2User oauth2User) {
            claims = oauth2User.getAttributes();
        } else {
            claims = Collections.emptyMap();
        }

        return new HashMap<>(claims);
    }
}
