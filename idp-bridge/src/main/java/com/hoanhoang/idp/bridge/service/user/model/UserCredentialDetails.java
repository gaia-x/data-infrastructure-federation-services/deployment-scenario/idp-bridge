package com.hoanhoang.idp.bridge.service.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;

public interface UserCredentialDetails {

    String userCredentialDetailsImpl();
    JsonNode loadCredentialSubject();
    String getUsername();
}
