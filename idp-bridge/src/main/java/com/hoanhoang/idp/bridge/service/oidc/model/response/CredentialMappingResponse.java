package com.hoanhoang.idp.bridge.service.oidc.model.response;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CredentialMappingResponse {

    private String claim;
    private String claimPath;
    private String credentialType;
    private String credentialFormat;
}
