package com.hoanhoang.idp.bridge.controller.wallet;


import com.hoanhoang.idp.bridge.service.wallet.WalletService;
import com.hoanhoang.idp.bridge.service.wallet.model.WalletProfileDto;
import com.hoanhoang.idp.bridge.service.wallet.rest.WalletProfileRequest;
import com.hoanhoang.idp.bridge.service.wallet.rest.WalletProfileResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.print.attribute.standard.Media;
import java.util.List;


@Tag(
        name = "Wallet Management API",
        description = "Manage Wallet profiles which are supported by IDP Bridge"
)
@Slf4j
@RequestMapping("/wallets")
@RestController("idpWalletController")
@RequiredArgsConstructor
public class WalletController {

    private final WalletService walletService;
    private final ModelMapper modelMapper;


    @Operation(
            summary = "Create a new wallet profile supported by IDP Bridge",
            description = "Create a new wallet profile supported by IDP Bridge"
    )
    @PostMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE }
    )
    public ResponseEntity<?> createWalletProfile(@RequestBody @Valid WalletProfileRequest walletRequest){

        WalletProfileDto walletDto = modelMapper.map(walletRequest, WalletProfileDto.class);

        WalletProfileDto createdWalletDto = walletService.createWalletProfile(walletDto);
        WalletProfileResponse createdWallet  = modelMapper.map(createdWalletDto, WalletProfileResponse.class);

        return new ResponseEntity<>(createdWallet, HttpStatus.CREATED);
    }


    @Operation(
            summary = "Find profile details of a Wallet by Id or Wallet Provider",
            description = "Find profile details of a Wallet by Id or Wallet Provider"
    )
    @GetMapping(
            path = "/profiles",
            produces = { MediaType.APPLICATION_JSON_VALUE }
    )
    public ResponseEntity<?> findWalletProfileById(@RequestParam(name = "walletId", required = false) Long walletId,
                                                   @RequestParam(name = "walletType", required = false) String walletProvider){

        if(walletId == null && walletProvider == null){
            log.error("At least walletID or Wallet Type must be specified");
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, "At least walletId or Wallet Type must be specified");
        }
        WalletProfileDto walletProfileDto = walletService.findWalletProfileByIdOrProvider(walletId, walletProvider);
        WalletProfileResponse response = modelMapper.map(walletProfileDto, WalletProfileResponse.class);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @Operation(
            summary = "Find profile details of all wallets supported by IDP Bridge",
            description = "Find profile details of all wallets supported by IDP Bridge"
    )
    @GetMapping
    public ResponseEntity<?> getAllWalletProfiles(){
        List<WalletProfileDto> walletDtos = walletService.findAllWalletProfiles();
        List<WalletProfileResponse> walletResponse = walletDtos.stream()
                .map(walletDto -> modelMapper.map(walletDto, WalletProfileResponse.class)).toList();

        return new ResponseEntity<>(walletResponse, HttpStatus.OK);
    }

    @Operation(
            summary = "Delete a wallet profile which is no longer supported by IDP Bridge",
            description = "Delete a wallet profile which is no longer supported by IDP Bridge"
    )
    @DeleteMapping()
    public ResponseEntity<?> deleteWalletProfile(@RequestParam(name = "walletId", required = true) Long walletId,
                                                 @RequestParam(name = "provider", required = false) String provider){
        if(walletId == null && provider == null){
            log.error("At least walletID or Wallet Type must be specified");
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "At least walletId or Wallet Type must be specified");
        }

        walletService.deleteWalletProfile(walletId, provider);
        log.info("Successfully deleted !");

        return ResponseEntity.ok().build();
    }
}
