package com.hoanhoang.idp.bridge.service.aster;


import com.hoanhoang.idp.bridge.authorizationserver.token.OAuthAsterTokenService;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;


@Service
@Slf4j
@RequiredArgsConstructor
public class AsterXVpTokenService {

    private final OAuthAsterTokenService oAuthAsterTokenService;


    public String issueVPToken(String iss, String subject, List<Map<String, Object>> vcs){

        VerifiablePresentationModel verifiablePresentationModel = new VerifiablePresentationModel();
        verifiablePresentationModel.setVerifiableCredential(new HashSet<>(vcs));

        return oAuthAsterTokenService.issueVPToken(iss, subject, verifiablePresentationModel);
    }


}
