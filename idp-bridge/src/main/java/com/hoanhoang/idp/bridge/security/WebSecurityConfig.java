package com.hoanhoang.idp.bridge.security;

import com.hoanhoang.idp.bridge.authorizationserver.token.OAuthUserInfoMapperService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configuration.OAuth2AuthorizationServerConfiguration;
import org.springframework.security.oauth2.server.authorization.config.annotation.web.configurers.OAuth2AuthorizationServerConfigurer;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.*;

@Configuration
@EnableWebSecurity
@Slf4j
@RequiredArgsConstructor
public class WebSecurityConfig {

    private static final String[] SECURITY_WHITE_LIST = {
            "/swagger-ui.html", "/swagger-ui/**" , "/v3/api-docs", "/v3/api-docs/**",
            "/actuator/**",  "/actuator",
            "/oidc/**",
            "/oauth2/jwks/**", "/.well-known/openid-configuration",
            "/openid4vp/verify", "/openid4vp/verify/**", "/openid4vp/verify","/openid4vp/qr", "/openid4vp/qr/**",
            "/openid4vp/session/**", "/openid4vp/**",
            "/openid4vp/continue/**", "/openid4vp/continue",
            "/openid4vp/qr", "/openid4vp/**", "/openid4vp/qr/**", "/openid4vp/qr",
            "/home", "/home/**", "/favicon.ico", "/favicon.ico/**","/error",
            "/crypto/**","/crypto",
            "/users", "/users/**",
            "/resources/**", "/static/**", "/webjars", "/webjars/**", "/assets/**", "/openid4vc/webjars/**", "/home/webjars", "/home/webjars/**"
    };

    @Value("${server.base-url}")
    private String serverBaseUrl;


//    @Bean
//    @Order(1)
//    public SecurityFilterChain authorizationServerSecurityFilterChain(HttpSecurity http) throws Exception {
//
////        // Enabling OIDC 1.0 protocol
////        OAuth2AuthorizationServerConfiguration.applyDefaultSecurity(http);
////        http.getConfigurer(OAuth2AuthorizationServerConfigurer.class)
////                .oidc(Customizer.withDefaults());
//
//        OAuth2AuthorizationServerConfigurer oAuth2AuthorizationServerConfigurer = new OAuth2AuthorizationServerConfigurer();
//        RequestMatcher endpointsMatcher = oAuth2AuthorizationServerConfigurer.getEndpointsMatcher();
//
//
//        Function<OidcUserInfoAuthenticationContext, OidcUserInfo> userInfoMapper = (context) -> {
//            OidcUserInfoAuthenticationToken authenticationToken = context.getAuthentication();
//            JwtAuthenticationToken principal = (JwtAuthenticationToken) authenticationToken.getPrincipal();
//
//            return new OidcUserInfo(principal.getToken().getClaims());
//        };
//
//        oAuth2AuthorizationServerConfigurer.oidc(oidc -> oidc.userInfoEndpoint(userInfo ->userInfo.userInfoMapper(userInfoMapper)));
//
////        http.getConfigurer(OAuth2AuthorizationServerConfigurer.class)
////                .oidc(oidc -> oidc.clientRegistrationEndpoint(clientEndpoint -> {
////                    clientEndpoint.authenticationProviders(SSIClientMetadataConfig.configureCustomClientMetadataConverters());
////                }));
//        http.securityMatcher(endpointsMatcher)
//                        .authorizeHttpRequests( request -> request.)
//        http
//                .
//                exceptionHandling(exception -> exception.authenticationEntryPoint(new SSIAuthenticationEntryPoint(serverBaseUrl + "/home")))
//                .oauth2ResourceServer(resourceServer -> resourceServer.jwt(Customizer.withDefaults()));       // Enable userInfo endpoint
//
//        return http.build();
//    }


    private final OAuthUserInfoMapperService oAuthUserInfoMapperService;


    @Bean
    @Order(1)
    public SecurityFilterChain authorizationServerSecurityFilterChain(HttpSecurity http) throws Exception {

        // Enabling OIDC 1.0 protocol
        OAuth2AuthorizationServerConfiguration.applyDefaultSecurity(http);
        http.getConfigurer(OAuth2AuthorizationServerConfigurer.class)
//                .oidc(Customizer.withDefaults());
        .oidc(oidc -> oidc
                .userInfoEndpoint(userInfo ->
                        userInfo.userInfoMapper(oAuthUserInfoMapperService)));

//        http.getConfigurer(OAuth2AuthorizationServerConfigurer.class)
//                .oidc(oidc -> oidc.clientRegistrationEndpoint(clientEndpoint -> {
//                    clientEndpoint.authenticationProviders(SSIClientMetadataConfig.configureCustomClientMetadataConverters());
//                }));

        http.exceptionHandling(exception -> exception.authenticationEntryPoint(new SSIAuthenticationEntryPoint(serverBaseUrl + "/login")))
                .oauth2ResourceServer(resourceServer -> resourceServer.jwt(Customizer.withDefaults()));

        return http.build();
    }


    @Bean
    @Order(2)
    SecurityFilterChain configureSecurityFilterChain(HttpSecurity http) throws Exception {

        http.csrf(AbstractHttpConfigurer::disable);
        http.cors(cors -> cors.configurationSource(corsConfiguration()));

        http.authorizeHttpRequests(request -> request
                        .requestMatchers(SECURITY_WHITE_LIST).permitAll()
                        .anyRequest().permitAll())
//                .formLogin(Customizer.withDefaults())
                .formLogin(form -> form
                        .loginPage("/login")
                        .failureUrl("/login-error"));
//                .oauth2Login(oauth2 -> oauth2
//                        .loginPage("/login")
//                        .successHandler(createFederatedIdentityAuthenticationSuccessHandler()));
        return http.build();

    }


//    @Bean
//    public OAuthUserInfoMapperService createUserInfoMapperService(){
//        return new OAuthUserInfoMapperService();
//    }

    @Bean
    public RequestCache requestCache(){
        return new HttpSessionRequestCache();
    }


    @Bean
    public CorsConfigurationSource corsConfiguration(){
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOriginPatterns(List.of("*"));
        config.setAllowedMethods(List.of("*"));
        config.setAllowedHeaders(List.of("*"));
        source.registerCorsConfiguration("/**", config);
        return source;
    }


    // Sample User Details Service for user/password authentication demonstration
    // Will be deprecated once IDP Bridge switches to LDAP
    @Bean
    public UserDetailsService users() {
        // Default users for test purpose in the user/password authentication
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        UserDetails user = User.withUsername("hoan")
                .password(encoder.encode("password"))
                .roles("USER")
                .build();
        return new InMemoryUserDetailsManager(user);
    }

}