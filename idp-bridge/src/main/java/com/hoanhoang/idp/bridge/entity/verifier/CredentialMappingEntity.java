package com.hoanhoang.idp.bridge.entity.verifier;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(
        name = "ssi_oidc_default_credential_mappings"
)
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CredentialMappingEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 6313304391207085209L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String claim;

    @Column(nullable = false)
    private String claimPath;

    @Column(nullable = false)
    private String credentialType;

    @Column(nullable = false)
    private String credentialFormat;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "scopes_vc_mapping",
            joinColumns = { @JoinColumn(name = "vc_mapping_id", referencedColumnName = "id")},
            inverseJoinColumns = { @JoinColumn(name = "scope_id", referencedColumnName = "id")}
    )
    private Set<ScopeEntity> scopeEntities = new HashSet<>();
}