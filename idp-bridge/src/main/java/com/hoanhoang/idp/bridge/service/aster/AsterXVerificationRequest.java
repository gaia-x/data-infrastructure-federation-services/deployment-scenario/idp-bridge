package com.hoanhoang.idp.bridge.service.aster;


import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AsterXVerificationRequest {


    @NotNull
    private String credential;
}
