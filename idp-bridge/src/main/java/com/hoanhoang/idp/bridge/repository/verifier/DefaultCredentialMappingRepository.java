package com.hoanhoang.idp.bridge.repository.verifier;

import com.hoanhoang.idp.bridge.entity.verifier.CredentialMappingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DefaultCredentialMappingRepository extends JpaRepository<CredentialMappingEntity, Long> {

}
