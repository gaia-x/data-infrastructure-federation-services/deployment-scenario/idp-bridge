package com.hoanhoang.idp.bridge.entity.wallet;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(
        name = "ssi_wallets"
)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WalletEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 7516685641283209796L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true, length = 100)
    private String provider;

    @Column(length = 1024)
    private String description;

    @ElementCollection
    @CollectionTable(name = "ssi_wallet_base_urls")
    private Set<String> baseUrls = new HashSet<>();
}
