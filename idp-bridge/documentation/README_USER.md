## User Service Manager
User Service Manager is responsible for retrieving user information during a verifiable credential issuance.

### Default User Service Provider
By default, IDP Bridge comes with a default implementation of User Service & In-memory User Repository with a minimal user model:

- In-memory User Storage: responsible for storing user details
- User Service: responsible for retrieving user information based on username

### Custom User Service Provider
One can develop their own user service provider by implementing UserCredentialDetailsManager interface which is described as following:

````java
public interface UserCredentialDetailsProvider {

    UserCredentialDetails getUserCredentialDetails(String username);
}
````

At application startup, IDPBridge automatically detects such an implementation for use instead of using the default user service provider.
