## Authorization Server

The authorization server implements multiple specifications to support the OIDC Core protocol. It is the core component which helps IDP Bridge securely connect to traditional IDP as well as do necessary steps to trigger ssi-based authentication protocols with ssi wallets. The following describes at higher level the core components of Authorization Server:

### Dynamic Client Registration

IDP Bridge supports the Dynamic Client Registration specification [SPEC](https://openid.net/specs/openid-connect-registration-1_0.html) provided by OIDC Foundation to facilitate client registration procedure in case that OP and Client do not know each other beforehand. This feature can be ON/OFF depending on the configuration.

Otherwise, a client profile can also be created via a dedicated endpoint illustrated below:

```
POST /oidc/clients/register
{
    "client_name": "kc",   // name of the client
    "redirect_uris": [
        "http://127.0.0.1:8081/realms/master/broker/custom-idp/endpoint"    // Url to be used by IDP Bridge to send the tokens once User is successfully authenticated
    ],
        "scopes": [
        "openid", "profile"
    ]
}
```
By default, a successful response includes client credentials and clients authentication methods.

### Token Service
Upon successful user authentication via Authentication Manager with any supported authentication method, Token Service is in charge of creating a *id_token* and *access_token* which are subsequently returned to Client, which can be Keycloak. 
The information in these tokens are dependent on the granted scopes and user information profile. The former is associated to consent service where user can accept sharing required information whereas the latter is retrieved from user information manager.

In case of SSI-based authentication, Token Service extracts information in the verifiable credentials which are submitted by User Wallet to create such tokens.
In addition, there is always a <mark>userInfo</mark> which is at disposal of Client to retrieve user information according to granted scopes. The endpoint is secured by an *access_token* which is sent to Client earlier.



### User Session Management
IDP Bridge has a configurable session management where each user session is correctly distinguished. Once a user has successfully authenticated with IDP Bridge, the later creates an authenticated session and maintain it for a specific period of time. The duration of session is configurable.


### User Consent Service
IDP Bridge supports a user consent service to explicitly make user aware of which information they are sharing with relying party. In the OIDC standards, the information is associated to granted scopes. 

