## Authentication Manager
Authentication Manager is responsible for registering and managing different Authentication mechanisms. Every authentication workflow via IDPBridge will be handled by one or some authentication mechanisms.

Authentication Manager also allow aggregating multiple authentication mechanisms into a single authentication workflow.

### OIDC Brokering
As OIDC protocol is widely adopted and supported,the OIDC Brokering Service is then needed to interact with all of them.

### OID4VP/SIOPv2
The most important authentication mechanism supported by Authentication Manager is OID4VP/SIOPv2. This enables authentication via SSI Wallet.


### ID/Password Authentication
The most basic and widely-used authentication mechanism supported Authentication Manager is ID/Password. 

### PKI Card
PKI Card is more and more used on the daily basic due to its security and reliability. The support for this  mechanism is much needed.

### Authentication Aggregator
Aggregator Service allows composing and/or combining multiple authentication mechanisms into a single authentication workflow. This is a useful feature to make 2 and 3 factor authentication workflow.