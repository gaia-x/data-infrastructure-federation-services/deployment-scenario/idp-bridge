## Verifier Manager
### OID4VP & SIOPv2 protocols

### Policy Evaluation Service
IDP Bridge comes with a plugin system of verification policy. Each policy could implement a set of verification logics to verify the validity of VCs and VP presented by User Wallet. Upon launched, IDP Bridge registers automatically 3 policies, including:
- *signature policy*: to verify signatures opposed on VCs, VP
- *expiration*: to verify whether a presented VC is expired
- *presentation_definition*: to verify whether User Wallet present all required VCs defined in the presentation definition

Due to the modularity of the policy plugin system, one can easily implement a custom policy by creating a new class which extends <mark> JavaCredentialWrapperValidatorPolicy </mark> class and implements two methods:
- constructor() method: defines 4 parameters
- doVerify() method: where one has access to vp_token, authentication context, and custom arguments

Once such a class is correctly implemented, the system will automatically take it into account.

One example of such a custom policy could be found [EXAMPLE](/src/main/java/com/hoanhoang/idp/bridge/service/policy/custom/ExamplePolicy.java)